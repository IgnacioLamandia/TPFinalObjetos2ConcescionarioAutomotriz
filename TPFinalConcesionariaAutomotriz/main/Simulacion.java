import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import adjudicacion.ModalidadDeAdjudicacionPorMayorCobertura;
import adjudicacion.ModalidadDeAdjudicacionPorSorteo;
import adjudicacion.ModalidadDeAdjudicacion;
import aseguradora.Aseguradora;
import automovil.ModeloAutomovil;
import cliente.Cliente;
import cliente.ClienteEnPlan;
import concesionario.Concesionario;
import fabrica.Fabrica;
import fabrica.PlantaDeProduccion;
import financiamiento.FormaDeFinanciamiento;
import financiamiento.FormaDeFinanciamiento100;
import financiamiento.FormaDeFinanciamiento7030;
import planDeAhorro.PlanDeAhorro;

public class Simulacion {
	
	@SuppressWarnings("deprecation")
	public static void main(final String[] args) {
		
		//Modelos de autos:
		ModeloAutomovil modelo0 = new ModeloAutomovil("modelo0", new Date(2015,1,1), 5, true, 100000);
		ModeloAutomovil modelo1 = new ModeloAutomovil("modelo1", new Date(2015,2,2), 5, false, 200000);
		ModeloAutomovil modelo2 = new ModeloAutomovil("modelo2", new Date(2015,3,3), 4, true, 300000);
        
		//Fabrica y Plantas:
		Fabrica fabrica = new Fabrica();
		
		PlantaDeProduccion planta0 = new PlantaDeProduccion();
		PlantaDeProduccion planta1 = new PlantaDeProduccion();
		PlantaDeProduccion planta2 = new PlantaDeProduccion();
		
		planta0.register(fabrica);
		planta2.register(fabrica);
		planta1.register(fabrica);
		
		fabrica.addPlantaDeProduccion(planta0);
		fabrica.addPlantaDeProduccion(planta1);
		fabrica.addPlantaDeProduccion(planta2);
		
		fabrica.addModeloAutomovil(modelo0);
		fabrica.addModeloAutomovil(modelo1);
		fabrica.addModeloAutomovil(modelo2);
		
		planta0.addStock(modelo0,0);
		planta0.addStock(modelo1,10);
		planta0.addStock(modelo2,20);
		planta1.addStock(modelo0,30);
		planta1.addStock(modelo1,40);
		planta1.addStock(modelo2,50);
		planta2.addStock(modelo0,60);
		planta2.addStock(modelo1,70);
		planta2.addStock(modelo2,80);

		//Planes de ahorro:
		FormaDeFinanciamiento financiamiento0 = new FormaDeFinanciamiento100(100);
		FormaDeFinanciamiento financiamiento1 = new FormaDeFinanciamiento7030(200);
		FormaDeFinanciamiento financiamiento2 = new FormaDeFinanciamiento100(300);
		
		ModalidadDeAdjudicacion modalidad0 = new ModalidadDeAdjudicacionPorSorteo();
		ModalidadDeAdjudicacion modalidad1 = new ModalidadDeAdjudicacionPorMayorCobertura();
		ModalidadDeAdjudicacion modalidad2 = new ModalidadDeAdjudicacionPorMayorCobertura();
		
		PlanDeAhorro plan0 = new PlanDeAhorro(0, modelo0, financiamiento0, modalidad0);
		PlanDeAhorro plan1 = new PlanDeAhorro(1, modelo1, financiamiento1, modalidad1);
		PlanDeAhorro plan2 = new PlanDeAhorro(2, modelo2, financiamiento2, modalidad2);
		
		//Clientes:
		Cliente c0 = new Cliente("nombre0", "apellido0", "00000000", new Date(1950,10,10), "direccion0", "mail0", new Date(2000,10,10));
		Cliente c1 = new Cliente("nombre1", "apellido1", "11111111", new Date(1951,1,1), "direccion1", "mail1", new Date(2001,1,1));
		Cliente c2 = new Cliente("nombre2", "apellido2", "22222222", new Date(1952,2,2), "direccion2", "mail2", new Date(2002,2,2));
		Cliente c3 = new Cliente("nombre3", "apellido3", "33333333", new Date(1953,3,3), "direccion3", "mail3", new Date(2003,3,3));
		Cliente c4 = new Cliente("nombre4", "apellido4", "44444444", new Date(1954,4,4), "direccion4", "mail4", new Date(2004,4,4));
		Cliente c5 = new Cliente("nombre5", "apellido5", "55555555", new Date(1955,5,5), "direccion5", "mail5", new Date(2005,5,5));
		Cliente c6 = new Cliente("nombre6", "apellido6", "66666666", new Date(1956,6,6), "direccion6", "mail6", new Date(2006,6,6));
		Cliente c7 = new Cliente("nombre7", "apellido7", "77777777", new Date(1957,7,7), "direccion7", "mail7", new Date(2007,7,7));
		Cliente c8 = new Cliente("nombre8", "apellido8", "88888888", new Date(1958,8,8), "direccion8", "mail8", new Date(2008,8,8));
		Cliente c9 = new Cliente("nombre9", "apellido9", "99999999", new Date(1959,9,9), "direccion9", "mail9", new Date(2009,9,9));
		
		ClienteEnPlan cliente0 = new ClienteEnPlan(c0, new Date(2016,10,10));
		ClienteEnPlan cliente1 = new ClienteEnPlan(c1, new Date(2016,1,1));
		ClienteEnPlan cliente2 = new ClienteEnPlan(c2, new Date(2016,2,2));
		ClienteEnPlan cliente3 = new ClienteEnPlan(c3, new Date(2016,3,3));
		ClienteEnPlan cliente4 = new ClienteEnPlan(c4, new Date(2016,4,4));
		ClienteEnPlan cliente5 = new ClienteEnPlan(c5, new Date(2016,5,5));
		ClienteEnPlan cliente6 = new ClienteEnPlan(c6, new Date(2016,6,6));
		ClienteEnPlan cliente7 = new ClienteEnPlan(c7, new Date(2016,7,7));
		ClienteEnPlan cliente8 = new ClienteEnPlan(c8, new Date(2016,8,8));
		ClienteEnPlan cliente9 = new ClienteEnPlan(c9, new Date(2016,9,9));
		
		plan0.agregarParticipante(cliente0);
		plan1.agregarParticipante(cliente1);
		plan2.agregarParticipante(cliente2);
		plan0.agregarParticipante(cliente3);
		plan1.agregarParticipante(cliente4);
		plan2.agregarParticipante(cliente5);
		plan0.agregarParticipante(cliente6);
		plan1.agregarParticipante(cliente7);
		plan2.agregarParticipante(cliente8);
		plan1.agregarParticipante(cliente9);
		
		cliente0.setCuotasPagadas(10);
		cliente1.setCuotasPagadas(11);
		cliente2.setCuotasPagadas(12);
		cliente3.setCuotasPagadas(13);
		cliente4.setCuotasPagadas(14);
		cliente5.setCuotasPagadas(15);
		cliente6.setCuotasPagadas(16);
		cliente7.setCuotasPagadas(17);
		cliente8.setCuotasPagadas(18);
		cliente9.setCuotasPagadas(19);
		
		//Aseguradora:
		
		Aseguradora aseguradora = new Aseguradora(50, 50, 10, 5);

		//CONCESCIONARIO:
		Concesionario concesionario = new Concesionario(fabrica, aseguradora);
		
		concesionario.agregarCliente(c0);
		concesionario.agregarCliente(c1);
		concesionario.agregarCliente(c2);
		concesionario.agregarCliente(c3);
		concesionario.agregarCliente(c4);
		concesionario.agregarCliente(c5);
		concesionario.agregarCliente(c6);
		concesionario.agregarCliente(c7);
		concesionario.agregarCliente(c8);
		concesionario.agregarCliente(c9);
		
		concesionario.agregarPlanDeAhorro(plan0);
		concesionario.agregarPlanDeAhorro(plan1);
		concesionario.agregarPlanDeAhorro(plan2);
		
		Map<PlantaDeProduccion, Integer> distanciasPlantas = new HashMap<PlantaDeProduccion, Integer>();
		distanciasPlantas.put(planta0, 100);
		distanciasPlantas.put(planta1, 111);
		distanciasPlantas.put(planta2, 222);
		
		concesionario.setDistanciasAPlantas(distanciasPlantas);
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		//probar planesConMayorCantidadDeSuscriptos():
		System.out.println("Probando planesConMayorCantidadDeSuscriptos():");
		System.out.println("numeroDeGrupo: " + concesionario.planesConMayorCantidadDeSuscriptos().get(0).getNumeroDeGrupo()); //plan1 (4)
		System.out.println("numeroDeGrupo: " + concesionario.planesConMayorCantidadDeSuscriptos().get(1).getNumeroDeGrupo()); //plan0 (3)
		System.out.println("numeroDeGrupo: " + concesionario.planesConMayorCantidadDeSuscriptos().get(2).getNumeroDeGrupo()); //plan2 (3)
		
		//probar getStockPorModelo():
		System.out.println("\nProbando getStockPorModelo():");
		System.out.println("Stock por modelo: " + concesionario.getStockTotalPorModelo());
		System.out.println("Hay stock de modelo0: " + concesionario.hayStockDeModelo(modelo0, 1));
		
		//probar ejecutarAdjudicacion():
		System.out.println("\nProbando ejecutarAdjudicacion():");
		concesionario.ejecutarAdjudicacion(plan0);
		System.out.println("participantes plan0: " + plan0.getParticipantes()); //tiene 3 participantes
		System.out.println("cupones: " + concesionario.getCupones()); //tiene 1 cupones de adjudicacion
		
		//probar ejecutarAdjudicaciones():
		System.out.println("\nProbando ejecutarAdjudicaciones():");
		System.out.println("Antes de ejecutarAdjudicaciones():");
		System.out.println("participantes plan0: " + plan0.getParticipantes()); //tiene 2 participantes
		System.out.println("participantes paln1: " + plan1.getParticipantes()); //tiene 4 participantes
		System.out.println("participantes plan2: " + plan2.getParticipantes()); //tiene 3 participantes
		
		concesionario.ejecutarAdjudicaciones();
		
		System.out.println("Despues de ejecutarAdjudicaciones():");
		System.out.println("participantes plan0: " + plan0.getParticipantes()); //tiene 1 participantes
		System.out.println("participantes plan1: " + plan1.getParticipantes()); //tiene 3 participantes
		System.out.println("participantes plan2: " + plan2.getParticipantes()); //tiene 2 participantes
		System.out.println("cupones: " + concesionario.getCupones()); //tiene 4 cupones de adjudicacion
		
		//probar EfectuarPagoDeCuotaDeCliente():
		System.out.println("\nProbando efectuarPagoDeCuotaDeCliente:");
		System.out.println(cliente0.getCuotasPagadas()); //cuotas pagadas 10
		System.out.println("plan0 comprobantes: " + plan0.getComprobantesDePago()); //no hay comprobantes
		concesionario.efectuarPagoDeCuotaDeCliente(cliente0, plan0, new Date(2015, 10,10), 500);
		System.out.println("cliente0 coutas pagadas: " + cliente0.getCuotasPagadas());    //cuotas pagadas 11
		System.out.println("plan0 comprobantes: " + plan0.getComprobantesDePago());	//se agrega el nuevo coprobante
		
		System.out.println(concesionario.plantasConStockDeAutomovil(modelo0, 18));
		System.out.println("planta0 " + planta0.getStockPorModelo().get(modelo0));
		System.out.println("planta1 " + planta1.getStockPorModelo().get(modelo0));
		System.out.println("planta2 " + planta2.getStockPorModelo().get(modelo0));
    }
}
