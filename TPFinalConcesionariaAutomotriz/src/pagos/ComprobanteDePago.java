package pagos;

import java.util.Date;

import cliente.ClienteEnPlan;
import planDeAhorro.PlanDeAhorro;

public class ComprobanteDePago {
	
	private ClienteEnPlan participante;
	private Integer numeroDeCuota;
	private Date fechaDePago;
	private Integer alicuota;
	private Integer gastosAdministrativos;
	private Integer seguro;

	public ComprobanteDePago(ClienteEnPlan participante, PlanDeAhorro plan, Date fechaDePago, Integer gastosAdministrativos, Integer seguro) {
		this.participante = participante;
		this.numeroDeCuota = participante.getNumeroDeCuotaAPagar();
		this.fechaDePago = fechaDePago;
		this.alicuota = this.calcularAlicuota(plan);
		this.gastosAdministrativos = gastosAdministrativos;
		this.seguro = seguro;
	}

	//GETTERS y SETTERS:
	
	public ClienteEnPlan getParticipante() {
		return participante;
	}

	public void setParticipante(ClienteEnPlan participante) {
		this.participante = participante;
	}

	public Integer getNumeroDeCuota() {
		return numeroDeCuota;
	}

	public void setNumeroDeCuota(Integer numeroDeCuota) {
		this.numeroDeCuota = numeroDeCuota;
	}

	public Date getFechaDePago() {
		return fechaDePago;
	}

	public void setFechaDePago(Date fechaDePago) {
		this.fechaDePago = fechaDePago;
	}

	public Integer getAlicuota() {
		return alicuota;
	}

	public void setAlicuota(Integer alicuota) {
		this.alicuota = alicuota;
	}

	public Integer getGastosAdministrativos() {
		return gastosAdministrativos;
	}

	public void setGastosAdministrativos(Integer gastosAdministrativos) {
		this.gastosAdministrativos = gastosAdministrativos;
	}

	public Integer getSeguro() {
		return seguro;
	}

	public void setSeguro(Integer seguro) {
		this.seguro = seguro;
	}

	//CALCULOS:
	/*
	 * Proposito: Retorna la alicuota a pagar al momento de la entrega.
	 */
	public Integer calcularAlicuota(PlanDeAhorro plan) 
	{
		return plan.calcularAlicuota();
	}
}