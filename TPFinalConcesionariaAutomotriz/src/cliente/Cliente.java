package cliente;

import java.util.Calendar;
import java.util.Date;

public class Cliente {

	private String nombreCliente;
	private String apellidoCliente;
	private String dniCliente;
	private Date fechaDeNacimientoCliente;
	private String direccionCliente;
	private String mailCliente;
	private Date fechaDeIngresoCliente;	//fecha de ingreso a la concescionaria.
	
	public Cliente(String nombre, String apellido, String dni, Date fechaNacimiento, 
				   String direccion, String mail, Date fechaIngreso)
	{
		this.nombreCliente = nombre;
		this.apellidoCliente = apellido;
		this.dniCliente = dni;
		this.fechaDeNacimientoCliente = fechaNacimiento;
		this.direccionCliente = direccion;
		this.mailCliente = mail;
		this.fechaDeIngresoCliente = fechaIngreso;
	}
	
	//GETTERS y SETTERS:
	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getApellidoCliente() {
		return apellidoCliente;
	}

	public void setApellidoCliente(String apellidoCliente) {
		this.apellidoCliente = apellidoCliente;
	}

	public String getDniCliente() {
		return dniCliente;
	}

	public void setDniCliente(String dniCliente) {
		this.dniCliente = dniCliente;
	}

	public Date getFechaDeNacimientoCliente() {
		return fechaDeNacimientoCliente;
	}

	public void setFechaDeNacimientoCliente(Date fechaDeNacimientoCliente) {
		this.fechaDeNacimientoCliente = fechaDeNacimientoCliente;
	}

	public String getDireccionCliente() {
		return direccionCliente;
	}

	public void setDireccionCliente(String direccionCliente) {
		this.direccionCliente = direccionCliente;
	}

	public String getMailCliente() {
		return mailCliente;
	}

	public void setMailCliente(String mailCliente) {
		this.mailCliente = mailCliente;
	}

	public Date getFechaDeIngresoCliente() {
		return fechaDeIngresoCliente;
	}

	public void setFechaDeIngresoCliente(Date fechaDeIngresoCliente) {
		this.fechaDeIngresoCliente = fechaDeIngresoCliente;
	}

	/*
	 * Proposito: Retorna la edad del cliente.
	 */
	@SuppressWarnings("deprecation")
	public Integer calcularEdad() 
	{
	    Calendar fechaActual = Calendar.getInstance();

	    int anho = fechaActual.get(Calendar.YEAR) - this.fechaDeNacimientoCliente.getYear();
	    int mes =fechaActual.get(Calendar.MONTH) - this.fechaDeNacimientoCliente.getMonth();
	    int dia = fechaActual.get(Calendar.DATE) - this.fechaDeNacimientoCliente.getDay();

	    if(mes < 0 || (mes == 0 && dia < 0))
	    {
	        anho--;
	    }

	        return anho;
	}
}