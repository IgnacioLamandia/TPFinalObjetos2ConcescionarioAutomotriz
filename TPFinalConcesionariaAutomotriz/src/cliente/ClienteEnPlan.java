package cliente;

import java.util.Date;

import cliente.Cliente;

public class ClienteEnPlan {  //wrapper de cliente para utilizar en PlanDeAhorro
	
	private Cliente cliente;
	private Integer cuotasPagadas;
	private Date fechaIngresoPlan;

	public ClienteEnPlan(Cliente cliente, Date fechaIngresoPlan) 
	{
		this.cliente = cliente;
		this.cuotasPagadas = 0;
		this.fechaIngresoPlan = fechaIngresoPlan;
	}
	
	//GETTERS y SETTERS:

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Integer getCuotasPagadas() {
		return cuotasPagadas;
	}

	public void setCuotasPagadas(Integer cuotasPagadas) {
		this.cuotasPagadas = cuotasPagadas;
	}

	public Date getFechaIngresoPlan() {
		return fechaIngresoPlan;
	}

	public void setFechaIngresoPlan(Date fechaIngresoPlan) {
		this.fechaIngresoPlan = fechaIngresoPlan;
	}
	
	public Date getFechaDeIngresoCliente() {
		return this.cliente.getFechaDeIngresoCliente();
	}

	public Date getFechaDeNacimientoCliente() {
		return this.cliente.getFechaDeNacimientoCliente();
	}

	public Integer getNumeroDeCuotaAPagar() {
		return (this.cuotasPagadas + 1);
	}
	
	/*
	 * Proposito: Actualiza las cuotas pagadas.
	 */
	public void pagarCuota()
	{
		this.cuotasPagadas++;
	}

	/*
	 * Proposito: Retorna la edad del cliente.
	 */
	public Integer calcularEdad() {
		return this.cliente.calcularEdad();
	}

}
