package fabrica;
import java.util.HashMap;
import java.util.Map;

import automovil.ModeloAutomovil;

public class PlantaDeProduccion extends MiObservable {
	
	private Map<ModeloAutomovil,Integer> stockPorModelo;
	
	
	public PlantaDeProduccion()
	{
		this.stockPorModelo = new HashMap<ModeloAutomovil,Integer>();
	}
	
	//GETTERS y SETTERS:
	
	public Map<ModeloAutomovil, Integer> getStockPorModelo()
	{
		return this.stockPorModelo;	
	}
	
	public void setStockPorModelo(Map<ModeloAutomovil, Integer> stockPorModelo) {
		this.stockPorModelo = stockPorModelo;
	}
	
	public Integer stockDeModeloEnPlanta(ModeloAutomovil modelo)
	{
		return this.stockPorModelo.get(modelo);
	}

	//AGREGAR y QUITAR AUTOMOVIL:
	/*
	 * Proposito: Agregar un nuevo modelo.
	 */
	public void addModelo(ModeloAutomovil nuevoModelo)
	{
		this.stockPorModelo.put(nuevoModelo, 0);
	}
	
	/*
	 * Proposito: Agregar una cantidad de stock para un modelo.
	 */
	public void addStock(ModeloAutomovil modelo, Integer cantidad)
	{
		this.stockPorModelo.put(modelo, this.stockPorModelo.get(modelo) + cantidad);
		notifyObservers(modelo, cantidad);
	}
	
	/*
	 * Proposito: Quitar una cantidad de stock para un modelo.
	 */
	public void venderAutomovil(ModeloAutomovil modelo, Integer cantidad)
	{
		this.stockPorModelo.put(modelo, this.stockPorModelo.get(modelo) - cantidad);
		notifyObservers(modelo, -cantidad);
	}
	
	//NOTIFY:
	@Override
	public void notifyObservers(ModeloAutomovil modelo, Integer cantidad) 
	{
		for(Integer fabrica = 0; fabrica < observers.size(); fabrica++)
		{
			observers.get(fabrica).update(modelo, cantidad);
		}
	}

}