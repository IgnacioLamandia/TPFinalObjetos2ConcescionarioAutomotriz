package fabrica;

@SuppressWarnings("serial")
public class SinStockException extends Exception {
	
	public SinStockException()
	{
		super("No hay mas stock del modelo solicitado");
	}

}
