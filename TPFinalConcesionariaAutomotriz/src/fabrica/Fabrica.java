package fabrica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import automovil.ModeloAutomovil;

public class Fabrica implements MiObserver{
	
	private List<PlantaDeProduccion> plantasDeProduccion;
	private Map<ModeloAutomovil,Integer> stockTotalPorModelo;	
	
	public Fabrica()
	{
		this.plantasDeProduccion = new ArrayList<PlantaDeProduccion>();
		this.stockTotalPorModelo = new HashMap<ModeloAutomovil,Integer>();
	}
	
	//GETTERS y SETTERS:
	
	public List<PlantaDeProduccion> getPlantasDeProduccion() {
		return plantasDeProduccion;
	}
	
	public Map<ModeloAutomovil, Integer> getStockTotalPorModelo() {
		return stockTotalPorModelo;
	}

	public void setStockTotalPorModelo(Map<ModeloAutomovil, Integer> stockTotalPorModelo) {
		this.stockTotalPorModelo = stockTotalPorModelo;
	}

	public void setPlantasDeProduccion(List<PlantaDeProduccion> plantasDeProduccion) {
		this.plantasDeProduccion = plantasDeProduccion;
	}	
	
	//AGREGAR PLANTA	
	/*
	 * Proposito: Agrega nuevaPlantasDeProduccion a la lista de plantasDeProduccion.
	 */
	public void addPlantaDeProduccion(PlantaDeProduccion nuevaPlantasDeProduccion) {
		this.plantasDeProduccion.add(nuevaPlantasDeProduccion);
	}
	
	//AGREGAR MODELO
	/*
	 * Proposito: Agrega nuevoModelo a la lista de stockTotalPorModelo.
	 */
	public void addModeloAutomovil(ModeloAutomovil nuevoModelo)
	{
		this.stockTotalPorModelo.put(nuevoModelo, 0);
		
		/*for(int planta = 0; planta < this.plantasDeProduccion.size(); planta++ )
		{
			this.plantasDeProduccion.get(planta).addModelo(nuevoModelo);
		}*/
		
		this.plantasDeProduccion.stream().forEach(planta -> planta.addModelo(nuevoModelo));
	}
	
	//HAY STOCK
	/*
	 * Proposito: Retorna true si hay una cantidad de stock de un automovil.
	 */
	public Boolean hayStockDeModelo(ModeloAutomovil automovil, Integer cantidad) 
	{
		Boolean hayStock = false;
		if(this.stockTotalPorModelo.containsKey(automovil))
		{
			hayStock = this.stockTotalPorModelo.get(automovil) >= cantidad;
		}
		return hayStock;
	}
	
	//VENDER AUTOMOVIL
	/*
	 * Proposito: Realiza la venta de una cantidad de stock de un automovil.
	 */
	public void venderAutomovil(ModeloAutomovil automovil, Integer cantidad, PlantaDeProduccion planta) throws SinStockException
	{
		if(this.hayStockDeModelo(automovil, cantidad))
		{
			planta.venderAutomovil(automovil,cantidad);
			this.stockTotalPorModelo.put(automovil, this.stockTotalPorModelo.get(automovil) - cantidad);
		}
		else
		{
			throw new SinStockException();//No hay mas stock;
		}
	}

	//PLANTAS CON STOCK DE MODELO
	/*
	 * Proposito: Retorna la lista de plantas en las que hay una cantidad de stock de un automovil.
	 */
	public List<PlantaDeProduccion> plantasConStockDeModelo(ModeloAutomovil automovil, Integer cantidad) 
	{
		/*List<PlantaDeProduccion> plantasConStock = new ArrayList<PlantaDeProduccion>();
		for(int planta = 0; planta < this.plantasDeProduccion.size(); planta++ )
		{
			if(this.plantasDeProduccion.get(planta).stockDeModeloEnPlanta(automovil) >= cantidad)
			{
				plantasConStock.add(this.plantasDeProduccion.get(planta));
			}
		}
		return plantasConStock;*/
		return (this.plantasDeProduccion.stream().
				filter(planta -> planta.stockDeModeloEnPlanta(automovil) >= cantidad).
				collect(Collectors.toList()));
	}

	//UPDATE
	/*
	 * Proposito: Actualiza la informacion del stock cuando una planta actualiza el suyo.
	 */
	public void update(ModeloAutomovil modelo, Integer stockPorModelo)
	{		
		this.getStockTotalPorModelo().put(modelo, this.stockTotalPorModelo.get(modelo) + stockPorModelo);
	}
	
}
