package fabrica;

import java.util.ArrayList;
import java.util.List;

import automovil.ModeloAutomovil;

public abstract class MiObservable {
		
	protected List<MiObserver> observers = new ArrayList<MiObserver>();

	public void register(MiObserver observer)
	{
		this.observers.add(observer);
	}
		
	public void unregister(MiObserver observer)
	{
		this.observers.remove(observer);
	}

	public abstract void notifyObservers(ModeloAutomovil modelo, Integer cantidad);

	public List<MiObserver> getObservers() 
	{
		return observers;
	}

	public void setObservers(List<MiObserver> observers) 
	{
		this.observers = observers;
	}		
	
}