package fabrica;

import automovil.ModeloAutomovil;

public interface MiObserver 
{
	public void update(ModeloAutomovil modelo, Integer stockPorModelo);
}
