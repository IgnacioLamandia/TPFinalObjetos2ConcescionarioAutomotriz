package adjudicacion;

import java.util.List;

import cliente.ClienteEnPlan;

public interface ModalidadDeAdjudicacion {
	
	/*
	 * Proposito: Retorna el participante al que se le adjudicara el auto.
	 * Precondicion: La lista participantes no es vacia.
	 */
	public ClienteEnPlan seleccionarAdjudicatario(List<ClienteEnPlan> participantes);

}
