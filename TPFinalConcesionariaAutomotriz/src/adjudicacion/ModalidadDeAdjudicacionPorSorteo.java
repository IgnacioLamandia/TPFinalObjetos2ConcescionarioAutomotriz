package adjudicacion;

import java.util.List;
import java.util.Random;
import cliente.ClienteEnPlan;

public class ModalidadDeAdjudicacionPorSorteo implements ModalidadDeAdjudicacion{
	
	private Random sorteo;
	
	/*
	 * (non-Javadoc)
	 * @see adjudicacion.ModalidadDeAdjudicacion#seleccionarAdjudicatario(java.util.List)
	 */
	public ClienteEnPlan seleccionarAdjudicatario(List<ClienteEnPlan> participantes)
	{
		sorteo = new Random();
		int index = sorteo.nextInt(participantes.size());
		ClienteEnPlan adjudicatario = participantes.get(index);
		return adjudicatario;
	}
}