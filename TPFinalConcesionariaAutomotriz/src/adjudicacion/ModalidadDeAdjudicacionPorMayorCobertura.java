package adjudicacion;

import java.util.ArrayList;
import java.util.List;
import cliente.ClienteEnPlan;

public class ModalidadDeAdjudicacionPorMayorCobertura implements ModalidadDeAdjudicacion{

	/*
	 * (non-Javadoc)
	 * @see adjudicacion.ModalidadDeAdjudicacion#seleccionarAdjudicatario(java.util.List)
	 */
	
	public ClienteEnPlan seleccionarAdjudicatario(List<ClienteEnPlan> participantes)
	{
		List<ClienteEnPlan> adjudicatarios = new ArrayList<ClienteEnPlan>();
		adjudicatarios = mayorProporcionDePago(participantes);
		
		if(adjudicatarios.size() == 1)
		{
			return adjudicatarios.get(0);
		}
		
		else
		{
			adjudicatarios = mayorAntiguedad(adjudicatarios);
			if(adjudicatarios.size() == 1)
			{
				return adjudicatarios.get(0);
			}
			else
			{
				adjudicatarios = mayorAntiguedadEnPlan(adjudicatarios);
				return adjudicatarios.get(0);  //si hay mas de 1 agarra el primero
			}
		}
	}

	/*
	 * Proposito: Retorna una lista con los participantes que mayor proporcion de pago cubrieron.
	 * Precondicion: La lista participantes no es vacia.
	 */
	public List<ClienteEnPlan> mayorProporcionDePago(List<ClienteEnPlan> participantes) 
	{

		List<ClienteEnPlan> adjudicatarios = new ArrayList<ClienteEnPlan>();
		adjudicatarios.add(participantes.get(0));
	
		for(int cliente = 0; cliente < participantes.size(); cliente++)
		{
			if(participantes.get(cliente).getCuotasPagadas() == 
			   adjudicatarios.get(0).getCuotasPagadas())
			{
				adjudicatarios.add(participantes.get(cliente));
			}
			else
			{
				if(participantes.get(cliente).getCuotasPagadas() > 
				   adjudicatarios.get(0).getCuotasPagadas())
				{
					adjudicatarios.clear();
					adjudicatarios.add(participantes.get(cliente));
				}
			}
		}
		return adjudicatarios;
	}
	
	/*
	 * Proposito: Retorna una lista con los participantes con mayor antiguedad en la concescionaria.
	 * Precondicion: La lista participantes no es vacia.
	 */
	public List<ClienteEnPlan> mayorAntiguedad(List<ClienteEnPlan> participantes) 
	{
		List<ClienteEnPlan> adjudicatarios = new ArrayList<ClienteEnPlan>();
		adjudicatarios.add(participantes.get(0));
	
		for(int cliente = 0; cliente < participantes.size(); cliente++)
		{
			if(participantes.get(cliente).getFechaDeIngresoCliente().equals(
			   adjudicatarios.get(0).getFechaDeIngresoCliente()))
			{
				adjudicatarios.add(participantes.get(cliente));
			}
			else
			{
				if(participantes.get(cliente).getFechaDeIngresoCliente().before(
				   adjudicatarios.get(0).getFechaDeIngresoCliente()))
				{
					adjudicatarios.clear();
					adjudicatarios.add(participantes.get(cliente));
				}
			}
		}
		return adjudicatarios;
	}
	
	/*
	 * Proposito: Retorna una lista con los participantes con mayor antiguedad en el plan.
	 * Precondicion: La lista participantes no es vacia.
	 */
	public List<ClienteEnPlan> mayorAntiguedadEnPlan(List<ClienteEnPlan> participantes)
	{
		List<ClienteEnPlan> adjudicatarios = new ArrayList<ClienteEnPlan>();
		adjudicatarios.add(participantes.get(0));
	
		for(int cliente = 0; cliente < participantes.size(); cliente++)
		{
			if(participantes.get(cliente).getFechaIngresoPlan().equals( 
					adjudicatarios.get(0).getFechaIngresoPlan()))
			{
				adjudicatarios.add(participantes.get(cliente));
			}
			else
			{
				if(participantes.get(cliente).getFechaIngresoPlan().before(
				   adjudicatarios.get(0).getFechaIngresoPlan()))
				{
					adjudicatarios.clear();
					adjudicatarios.add(participantes.get(cliente));
				}
			}
		}
		return adjudicatarios;
	}
	
}
