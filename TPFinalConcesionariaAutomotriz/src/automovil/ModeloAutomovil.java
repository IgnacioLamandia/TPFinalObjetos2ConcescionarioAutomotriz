package automovil;

import java.util.Date;

public class ModeloAutomovil {
	
	private String nombreModelo;
	private Date anhoLanzamientoModelo;
	private Integer cantidadPuertas;
	private Boolean esBase; 			  //true es base, false es full.
	private Integer precio;
	
	public ModeloAutomovil(String nombreModelo, Date anhoLanzamientoModelo, 
						   Integer cantidadPuertas, Boolean esBase, Integer precio) 
	{
		this.nombreModelo = nombreModelo;
		this.anhoLanzamientoModelo = anhoLanzamientoModelo;
		this.cantidadPuertas = cantidadPuertas;
		this.esBase = esBase;
		this.precio = precio;
	}
	
	//GETTERS y SETTERS:

	public String getNombreModelo() {
		return nombreModelo;
	}

	public void setNombreModelo(String nombreModelo) {
		this.nombreModelo = nombreModelo;
	}

	public Date getAnhoLanzamientoModelo() {
		return anhoLanzamientoModelo;
	}

	public void setAnhoLanzamientoModelo(Date anhoLanzamientoModelo) {
		this.anhoLanzamientoModelo = anhoLanzamientoModelo;
	}

	public Integer getCantidadPuertas() {
		return cantidadPuertas;
	}

	public void setCantidadPuertas(Integer cantidadPuertas) {
		this.cantidadPuertas = cantidadPuertas;
	}

	public Boolean getEsBase() {
		return esBase;
	}

	public void setEsBase(Boolean esBase) {
		this.esBase = esBase;
	}

	public Integer getPrecio() {
		return precio;
	}

	public void setPrecio(Integer precio) {
		this.precio = precio;
	}	
}
