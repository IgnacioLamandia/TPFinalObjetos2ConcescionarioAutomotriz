package aseguradora;

import automovil.ModeloAutomovil;
import cliente.ClienteEnPlan;

public class Aseguradora {
	
	private Integer recargoFijoPorEdad;      //recargo fijo para cualquier edad
	private Integer edadLimiteSinRecargo;    //edad a partir de la cual comienza el extra
	private Integer extraPorEdad;			 //valor del extra por edad
	private Integer extraPorValorAutomovil;  //porcentaje extra al valor del automovil
	
	public Aseguradora(Integer recargoFijoPorEdad, Integer edadLimiteSinRecargo, 
					   Integer extraPorEdad, Integer extraPorValorAutomovil)
	{		
		this.recargoFijoPorEdad = recargoFijoPorEdad;
		this.edadLimiteSinRecargo = edadLimiteSinRecargo;
		this.extraPorEdad = extraPorEdad;
		this.extraPorValorAutomovil = extraPorValorAutomovil;
	}
	
	//GETTERS y SETTERS:
	public Integer getRecargoFijoPorEdad() {
		return recargoFijoPorEdad;
	}

	public void setRecargoFijoPorEdad(Integer recargoFijoPorEdad) {
		this.recargoFijoPorEdad = recargoFijoPorEdad;
	}

	public Integer getEdadLimiteSinRecargo() {
		return edadLimiteSinRecargo;
	}
	
	public void setEdadLimiteSinRecargo(Integer edadLimiteSinRecargo) {
		this.edadLimiteSinRecargo = edadLimiteSinRecargo;
	}

	public Integer getExtraPorEdad() {
		return extraPorEdad;
	}

	public void setExtraPorEdad(Integer extraPorEdad) {
		this.extraPorEdad = extraPorEdad;
	}

	public Integer getExtraPorValorAutomovil() {
		return extraPorValorAutomovil;
	}

	public void setExtraPorValorAutomovil(Integer extraPorValorAutomovil) {
		this.extraPorValorAutomovil = extraPorValorAutomovil;
	}

	//Calcular seguro:
	/*
	 * Proposito: Retorna el valor del seguro a pagar para participante que adquiere automovil.
	 */
	public Integer calcularSeguro(ClienteEnPlan participante, ModeloAutomovil automovil) 
	{
		Integer montoPorEdadParticipante = this.recargoFijoPorEdad;
		
		if(participante.calcularEdad() > this.edadLimiteSinRecargo)
		{
			montoPorEdadParticipante = montoPorEdadParticipante + 
									   (participante.calcularEdad() - this.edadLimiteSinRecargo) * this.extraPorEdad;
		}
		
		Integer montoPorValorAutomovil = (automovil.getPrecio() * this.extraPorValorAutomovil) / 100;
			
		return (montoPorEdadParticipante + montoPorValorAutomovil);
	}
}
