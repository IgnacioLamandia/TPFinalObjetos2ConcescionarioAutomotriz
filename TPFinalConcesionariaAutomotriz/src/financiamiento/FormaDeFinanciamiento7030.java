package financiamiento;

public class FormaDeFinanciamiento7030 extends FormaDeFinanciamiento{
	
	public FormaDeFinanciamiento7030(Integer cuotas)
	{
		super(cuotas);
		this.porcentajeEnEfectivo = 30;
		this.porcentajeEnCuotas = 70;	
	}
	/*
	 * (non-Javadoc)
	 * @see financiamiento.FormaDeFinanciamiento#calcularMontoAPagar(java.lang.Integer)
	 */
	public Integer calcularMontoAPagar(Integer precioAutomovil)
	{
		return ((precioAutomovil * 30) / 100);
	}
	/*
	 * (non-Javadoc)
	 * @see financiamiento.FormaDeFinanciamiento#calcularAlicuota(java.lang.Integer)
	 */
	public Integer calcularAlicuota(Integer precioAutomovil)
	{
		return (((precioAutomovil * 100) / 70) / cuotas);
	}
}
