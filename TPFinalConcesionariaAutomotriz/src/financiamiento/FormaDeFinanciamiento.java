package financiamiento;

public abstract class FormaDeFinanciamiento {
	
	protected Integer porcentajeEnEfectivo;
	protected Integer porcentajeEnCuotas;	
	protected Integer cuotas;
	
	public FormaDeFinanciamiento(Integer cuotas)
	{	
		this.cuotas = cuotas;
	}
	
	//GETTERS y SETTER:
	
	public Integer getPorcentajeEnEfectivo() {
		return porcentajeEnEfectivo;
	}
	
	public Integer getPorcentajeEnCuotas() {
		return porcentajeEnCuotas;
	}

	public Integer getCuotas() {
		return cuotas;
	}
	public void setCuotas(Integer cuotas) {
		this.cuotas = cuotas;
	}
	
	//CALCULOS:
	/*
	 * Proposito: Retorna el monto a pagar al momento de la entrega
	 */
	public abstract Integer calcularMontoAPagar(Integer precioAutomovil);
	/*
	 * Proposito: Retorna la alicuota a pagar al momento de la entrega.
	 */
	public abstract Integer calcularAlicuota(Integer precioAutomovil);

}
