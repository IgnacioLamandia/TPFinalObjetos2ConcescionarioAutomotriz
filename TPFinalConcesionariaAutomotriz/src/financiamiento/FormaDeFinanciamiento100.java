package financiamiento;

public class FormaDeFinanciamiento100 extends FormaDeFinanciamiento{
	
	public FormaDeFinanciamiento100(Integer cuotas)
	{
		super(cuotas);
		this.porcentajeEnEfectivo = 0;
		this.porcentajeEnCuotas = 100;	
	}
	/*
	 * (non-Javadoc)
	 * @see financiamiento.FormaDeFinanciamiento#calcularMontoAPagar(java.lang.Integer)
	 */
	public Integer calcularMontoAPagar(Integer precioAutomovil)
	{
		return (0);
	}
	/*
	 * (non-Javadoc)
	 * @see financiamiento.FormaDeFinanciamiento#calcularAlicuota(java.lang.Integer)
	 */
	public Integer calcularAlicuota(Integer precioAutomovil)
	{
		return (precioAutomovil / cuotas);
	}

}
