package planDeAhorro;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import adjudicacion.ModalidadDeAdjudicacion;
import automovil.ModeloAutomovil;
import cliente.ClienteEnPlan;
import financiamiento.FormaDeFinanciamiento;
import pagos.ComprobanteDePago;

public class PlanDeAhorro {
	
	private Integer numeroDeGrupo; //identificador
	private ModeloAutomovil modeloSuscripto;
	private List<ClienteEnPlan> participantes;
	private FormaDeFinanciamiento financiamiento;
	private ModalidadDeAdjudicacion modalidadDeAdjudicacion;
	private List<ComprobanteDePago> comprobantesDePago;

	public PlanDeAhorro(Integer numeroDeGrupo, ModeloAutomovil modeloSuscripto,
			FormaDeFinanciamiento financiamiento, ModalidadDeAdjudicacion modalidadDeAdjudicacion) 
	{
		this.numeroDeGrupo = numeroDeGrupo;
		this.modeloSuscripto = modeloSuscripto;
		this.participantes = new ArrayList<ClienteEnPlan>();
		this.financiamiento = financiamiento;
		this.modalidadDeAdjudicacion = modalidadDeAdjudicacion;
		this.comprobantesDePago = new ArrayList<ComprobanteDePago>();
	}

	//GETTERS y SETTERS:
	public Integer getNumeroDeGrupo() {
		return numeroDeGrupo;
	}

	public void setNumeroDeGrupo(Integer numeroDeGrupo) {
		this.numeroDeGrupo = numeroDeGrupo;
	}

	public ModeloAutomovil getModeloSuscripto() {
		return modeloSuscripto;
	}

	public void setModeloSuscripto(ModeloAutomovil modeloSuscripto) {
		this.modeloSuscripto = modeloSuscripto;
	}

	public List<ClienteEnPlan> getParticipantes() {
		return participantes;
	}

	public void setParticipantes(List<ClienteEnPlan> participantes) {
		this.participantes = participantes;
	}

	public FormaDeFinanciamiento getFinanciamiento() {
		return financiamiento;
	}

	public void setFinanciamiento(FormaDeFinanciamiento financiamiento) {
		this.financiamiento = financiamiento;
	}

	public ModalidadDeAdjudicacion getModalidadDeAdjudicacion() {
		return modalidadDeAdjudicacion;
	}

	public void setModalidadDeAdjudicacion(ModalidadDeAdjudicacion modalidadDeAdjudicacion) {
		this.modalidadDeAdjudicacion = modalidadDeAdjudicacion;
	}
	
	public List<ComprobanteDePago> getComprobantesDePago() {
		return comprobantesDePago;
	}
	
	public void setComprobantesDePago(List<ComprobanteDePago> comprobantesDePago) {
		this.comprobantesDePago = comprobantesDePago;
	}
	
	public Integer getTotalDeCuotas() {

		return this.financiamiento.getCuotas();
	}

	//AGREGAR PARTICIPANTE:
	/*
	 * Proposito: Agrega nuevoParticpante a la lista de participantes.
	 */
	public void agregarParticipante(ClienteEnPlan nuevoParticipante)
	{
		this.participantes.add(nuevoParticipante);
	}
	
	//AGREGAR COMPROBANTE:
	/*
	 * Proposito: Agrega nuevoComprobante a la lista de comprobantes.
	 */
		public void agregarComprobanteDePago(ComprobanteDePago nuevoComprobante)
		{
			this.comprobantesDePago.add(nuevoComprobante);
		}

	//SELECCIONAR ADJUDICATARIO:
	/*
	 * Proposito: Retorna el participante al que se le adjudicara el auto.
	 */
	public ClienteEnPlan seleccionarAdjudicatario()
	{
		ClienteEnPlan adjudicatario = this.modalidadDeAdjudicacion.seleccionarAdjudicatario(this.participantes);
		this.participantes.remove(adjudicatario);
		
		return adjudicatario;			
	}
	
	//CALCULAR MONTO A PAGAR 
	/*
	 * Proposito: Retorna el monto a pagar al momento de la entrega.
	 */
	public Integer calcularMontoAPagar()
	{
		return this.financiamiento.calcularMontoAPagar(this.modeloSuscripto.getPrecio());
	}
	
	//CALCULAR ALICUOTA:
	/*
	 * Proposito: Retorna la alicuota a pagar al momento de la entrega.
	 */
	public Integer calcularAlicuota()
	{
		return (this.financiamiento.calcularAlicuota(this.modeloSuscripto.getPrecio()));
	}
	
	//EMITIR COMPROBANTE:
	/*
	 * Proposito: Agrega un  nuevo comprobante de pago con un participante, una fechaDePago, gastosAdministrativos y un seguro.
	 */
	public void emitirComprobanteDePagoParaCliente(ClienteEnPlan participante, Date fechaDePago, Integer gastosAdministrativos, Integer seguro)
	{
		this.agregarComprobanteDePago(new ComprobanteDePago(participante, this, fechaDePago, gastosAdministrativos, seguro));

		participante.pagarCuota();
	}

}
