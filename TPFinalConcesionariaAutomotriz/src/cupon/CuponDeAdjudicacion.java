package cupon;

import automovil.ModeloAutomovil;
import cliente.ClienteEnPlan;

public class CuponDeAdjudicacion {
	
	private ClienteEnPlan adjudicatario; 
	private ModeloAutomovil automovil;
	private Integer montoAPagar;			//monto a pagar al momento de la entrega.
	
	public CuponDeAdjudicacion(ClienteEnPlan adjudicatario,  ModeloAutomovil auto, Integer montoAPagar){
		this.adjudicatario = adjudicatario;
		this.automovil = auto;
		this.montoAPagar = montoAPagar;
	}

	//GETTERS y SETTERS:
	public ClienteEnPlan getAdjudicatario() {
		return adjudicatario;
	}

	public void setAdjudicatario(ClienteEnPlan adjudicatario) {
		this.adjudicatario = adjudicatario;
	}

	public ModeloAutomovil getAutomovil() {
		return automovil;
	}

	public void setAutomovil(ModeloAutomovil automovil) {
		this.automovil = automovil;
	}

	public Integer getMontoAPagar() {
		return montoAPagar;
	}

	public void setMontoAPagar(Integer montoAPagar) {
		this.montoAPagar = montoAPagar;
	}

}
