package concesionario;

import fabrica.Fabrica;
import fabrica.PlantaDeProduccion;
import fabrica.SinStockException;
import planDeAhorro.PlanDeAhorro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import aseguradora.Aseguradora;
import automovil.ModeloAutomovil;
import cliente.Cliente;
import cliente.ClienteEnPlan;
import cupon.CuponDeAdjudicacion;

public class Concesionario {
	
	private Fabrica fabrica;
	private Map<PlantaDeProduccion,Integer> distanciasAPlantas;
	private List<Cliente> clientes;
	private List<PlanDeAhorro> planes;
	private List<CuponDeAdjudicacion> cupones;
	private Aseguradora aseguradora;
	
	public Concesionario(Fabrica fabrica, Aseguradora aseguradora)
	{
		this.fabrica  = fabrica;
		this.distanciasAPlantas = new HashMap<PlantaDeProduccion,Integer>();
		this.clientes = new ArrayList<Cliente>();
		this.planes   = new ArrayList<PlanDeAhorro>();
		this.cupones   = new ArrayList<CuponDeAdjudicacion>();
		this.aseguradora = aseguradora;
	}
	
	//GETTERS y SETTERS
	public Fabrica getFabrica() {
		return fabrica;
	}

	public void setFabrica(Fabrica fabrica) {
		this.fabrica = fabrica;
	}

	public Map<PlantaDeProduccion, Integer> getDistanciasAPlantas() {
		return distanciasAPlantas;
	}

	public void setDistanciasAPlantas(Map<PlantaDeProduccion, Integer> distanciasAPlantas) {
		this.distanciasAPlantas = distanciasAPlantas;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public List<PlanDeAhorro> getPlanes() {
		return planes;
	}

	public void setPlanes(List<PlanDeAhorro> planes) {
		this.planes = planes;
	}
	
	public List<CuponDeAdjudicacion> getCupones() {
		return cupones;
	}

	public void setCupones(List<CuponDeAdjudicacion> cupones) {
		this.cupones = cupones;
	}
	
	public Aseguradora getAseguradora() {
		return aseguradora;
	}

	public void setAseguradora(Aseguradora aseguradora) {
		this.aseguradora = aseguradora;
	}
	
	//AGREGAR
	/*
	 * Proposito: Agrega nuevoCliente a la lista de clientes.
	 */
	public void agregarCliente(Cliente nuevoCliente)
	{
		this.clientes.add(nuevoCliente);
	}
	/*
	 * Proposito: Agrega nuevoPlan a la lista de planes.
	 */
	public void agregarPlanDeAhorro(PlanDeAhorro nuevoPlan)
	{
		this.planes.add(nuevoPlan);
	}
	/*
	 * Proposito: Agrega nuevoCupon a la lista de cupones.
	 */
	public void agregarCupon(CuponDeAdjudicacion nuevoCupon)
	{
		this.cupones.add(nuevoCupon);
	}

	//10 planes con mas suscriptores:
	/*
	 * Proposito: Retorna una lista ordenada de mayor a menor con los (maximo 10) planes con mayor cantidad de suscriptos.
	 */
	public List<PlanDeAhorro> planesConMayorCantidadDeSuscriptos()
	{
		Collections.sort(this.planes, new Comparator<PlanDeAhorro>(){
			@Override
			public int compare(PlanDeAhorro p1, PlanDeAhorro p2) 
			{
				if(p1.getParticipantes().size() > p2.getParticipantes().size())
				{
					return -1;
				}
				else if(p1.getParticipantes().size() < p2.getParticipantes().size())
					 {
						 return 1;
					 }
					else
					{
						return 0;
					}
				}
				});
		
		List<PlanDeAhorro> mayores = new ArrayList<PlanDeAhorro>();
		
		for(int plan = 0; plan < this.planes.size() && plan < 10; plan++ )
		{
			
			mayores.add(this.planes.get(plan));
		}
		
		return mayores;
	}
	
	//Ejecutar adjudicacion
	/*
	 * Proposito: Ejecuta la adjudicacion de un plan en particular.
	 */
	public void ejecutarAdjudicacion(PlanDeAhorro plan)
	{
		try 
		{
			ClienteEnPlan adjudicatario = plan.seleccionarAdjudicatario();
			
			ModeloAutomovil modeloSuscripto = plan.getModeloSuscripto();
			
			PlantaDeProduccion planta;
			planta = this.plantaMasCercanaConStock(modeloSuscripto, 1);
			
			Integer montoAPagar = plan.calcularMontoAPagar() + this.calcularGastosDeFlete(planta);
			
			this.fabrica.venderAutomovil(modeloSuscripto, 1, planta);
			
			this.agregarCupon(new CuponDeAdjudicacion(adjudicatario, modeloSuscripto, montoAPagar));
		}
		
		catch (SinStockException e) 
		{
			e.getMessage();
		}
	}
	
	//Ejecutar adjudicaciones
	/*
	 * Proposito: Ejecuta la adjudicacion de todos los planes de la concescionaria.
	 */
	public void ejecutarAdjudicaciones()
	{/*
		for(int plan = 0; plan < this.planes.size(); plan++ )
		{
			this.ejecutarAdjudicacion(this.planes.get(plan));
		}
	 */
		this.planes.stream().forEach(plan -> this.ejecutarAdjudicacion(plan));
	}
	
	/*
	 * Proposito: Retorna la planta mas cercana con stock de modeloSuscripto.
	 */
	public PlantaDeProduccion plantaMasCercanaConStock(ModeloAutomovil automovil, Integer cantidad)
	{
			List<PlantaDeProduccion> plantasConStockDeAutomovil = this.plantasConStockDeAutomovil(automovil, cantidad);
			PlantaDeProduccion plantaMasCercanaConStock = plantasConStockDeAutomovil.get(0);
		
			for(int planta = 0; planta < plantasConStockDeAutomovil.size(); planta++ )
			{	
				if(this.distanciasAPlantas.get(plantaMasCercanaConStock) > 
				   this.distanciasAPlantas.get(plantasConStockDeAutomovil.get(planta)))
				{
					plantaMasCercanaConStock = plantasConStockDeAutomovil.get(planta);
				}
			}
			return plantaMasCercanaConStock;
	}
	
	/*
	 * Proposito: Retorna el gasto de flete para el modeloSuscripto.
	 */
	public Integer calcularGastosDeFlete(PlantaDeProduccion planta) 
	{
		Integer distancia = this.getDistanciasAPlantas().get(planta); 
		return distancia; //ver como se calcula el gasto en base a la distancia
	}
	
	/*
	 * Proposito: Retorna la lista de plantas que tienen stock de modeloSuscripto.
	 */
	public List<PlantaDeProduccion> plantasConStockDeAutomovil(ModeloAutomovil automovil, Integer cantidad) 
	{
		/*if(this.hayStockDeModelo(modeloSuscripto, cantidad));
		{
		Set<PlantaDeProduccion> plantas = this.distanciasAPlantas.keySet();		
		return (plantas.stream().
								filter(planta -> planta.stockDeModeloEnPlanta(modeloSuscripto) > 0).
								collect(Collectors.toList()));
		}
		*/
		return this.fabrica.plantasConStockDeModelo(automovil, cantidad);
	}

	/*
	 * Proposito: Retorna si hay stock de modeloSuscripto.
	 */
	public Boolean hayStockDeModelo(ModeloAutomovil automovil, Integer cantidad)
	{
		return this.fabrica.hayStockDeModelo(automovil, cantidad);
	}
	
	/*
	 * Proposito: Retorna el stock total de todos los modelos fabricados.
	 */
	public Map<ModeloAutomovil,Integer> getStockTotalPorModelo()
	{
		return this.fabrica.getStockTotalPorModelo();
	}
	
	/*
	 * Proposito: Efectua el pago de una cuota por parte de un cliente participante con fechaDePago y los correspondientes gastosAdministrativos.
	 * Precondicion: El participante debe pertenecer al plan.
	 */
	public void efectuarPagoDeCuotaDeCliente(ClienteEnPlan participante, PlanDeAhorro plan, Date fechaDePago, Integer gastosAdministrativos)
	{
		Integer seguro = aseguradora.calcularSeguro(participante, plan.getModeloSuscripto());
		
		plan.emitirComprobanteDePagoParaCliente(participante , fechaDePago, gastosAdministrativos, seguro);
	}
}
