package fabricaTestCase;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import automovil.ModeloAutomovil;
import fabrica.Fabrica;
import fabrica.PlantaDeProduccion;
import fabrica.SinStockException;

//SUT = Fabrica

public class FabricaTestCase {
	
	private Fabrica fabrica;

	@Before
	public void setUp() throws Exception {
		
		fabrica = new Fabrica();
	}
	
	//TESTS GETTERS y SETTERS:
	@SuppressWarnings("unchecked")
	@Test
	public void testGetYSetPlantasDeProduccion() {
		List<PlantaDeProduccion> plantas = mock(List.class);
				
		fabrica.setPlantasDeProduccion(plantas);
		assertEquals(fabrica.getPlantasDeProduccion(), plantas);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testGetYSetStockTotalPorModelo() {
		Map<ModeloAutomovil, Integer> stock = mock(Map.class);
				
		fabrica.setStockTotalPorModelo(stock);
		assertEquals(fabrica.getStockTotalPorModelo(), stock);	
	}

	//TESTS DE AGREGACION
	/*
	 * Proposito: Testear que se agrega una planta a la lista de plantasDeProduccion.
	 */
	@Test
	public void testAddPlantaDeProduccion() {
		
		PlantaDeProduccion planta = mock(PlantaDeProduccion.class);
		
		fabrica.addPlantaDeProduccion(planta);
		
		assertTrue(fabrica.getPlantasDeProduccion().contains(planta));
		
	}
	
	/*
	 * Proposito: Testear que se agrega un automovil al map de modelos y stock.
	 */
	@Test
	public void testAddModeloAutomovil() {
		
		ModeloAutomovil auto = mock(ModeloAutomovil.class);
		List<PlantaDeProduccion> plantas = new ArrayList<PlantaDeProduccion>();
		PlantaDeProduccion planta = mock(PlantaDeProduccion.class);
		plantas.add(planta);
		fabrica.setPlantasDeProduccion(plantas);
		
		fabrica.addModeloAutomovil(auto);
		
		assertTrue(fabrica.getStockTotalPorModelo().containsKey(auto));
		
	}
	
	/*
	 * Proposito: Testear que hay una cantidad determinada de stock de un modelo particular.
	 */
	@Test
	public void testHayStockDeModelo() {
		
		ModeloAutomovil auto = mock(ModeloAutomovil.class);
		Map<ModeloAutomovil, Integer> stock = new HashMap<ModeloAutomovil, Integer>();
		stock.put(auto, 0);
		
		assertFalse(fabrica.hayStockDeModelo(auto,1));//NO hay stock
		
		stock.put(auto, 5);
		fabrica.setStockTotalPorModelo(stock);
		
		assertTrue(fabrica.hayStockDeModelo(auto,1)); //Hay stock
	}

	/*
	 * Proposito: Testear que hay plantas con una cantidad determinada de stock de un modelo particular.
	 */
	@Test
	public void testPlantasConStockDeModelo() {
		
		List<PlantaDeProduccion> plantas = new ArrayList<PlantaDeProduccion>();
		PlantaDeProduccion planta1 = mock(PlantaDeProduccion.class);
		PlantaDeProduccion planta2 = mock(PlantaDeProduccion.class);
		PlantaDeProduccion planta3 = mock(PlantaDeProduccion.class);
		
		plantas.add(planta1);
		plantas.add(planta2);
		plantas.add(planta3);
		
		fabrica.setPlantasDeProduccion(plantas);
		
		ModeloAutomovil auto = mock(ModeloAutomovil.class);
		when(planta1.stockDeModeloEnPlanta(auto)).thenReturn(2);
		when(planta2.stockDeModeloEnPlanta(auto)).thenReturn(8);
		when(planta3.stockDeModeloEnPlanta(auto)).thenReturn(5);
		
		List<PlantaDeProduccion> result = fabrica.plantasConStockDeModelo(auto, 5);
		List<PlantaDeProduccion> expected = new ArrayList<PlantaDeProduccion>();
		expected.add(planta2);
		expected.add(planta3);
		
		assertEquals(expected,result);
	}
	
	/*
	 * Proposito: Testear que se puede vender una cantidad determinada de un modelo particular.
	 * Comprobar que no se lanza SinStockException.
	 */
	@Test
	public void testVenderAutomovilNoLanzaExcepcion() throws SinStockException {
		try
		{
			ModeloAutomovil auto = mock(ModeloAutomovil.class);
			PlantaDeProduccion planta = mock(PlantaDeProduccion.class);
			Map<ModeloAutomovil, Integer> stock = new HashMap<ModeloAutomovil, Integer>();
			stock.put(auto, 5);
			fabrica.setStockTotalPorModelo(stock);

			fabrica.venderAutomovil(auto, 2, planta);
		
			assertTrue(fabrica.getStockTotalPorModelo().get(auto).equals(3));
		
		}
		catch (SinStockException e) {
		    fail("Lanzada excepcion no esperada: SinStockException"); 	//verificar que no lanza excepcion
		}
	
	}
	
	/*
	 * Proposito: Testear que no se puede vender una cantidad determinada de un modelo particular.
	 * Comprobar que se lanza SinStockException.
	 */
	@Test
	public void testVenderAutomovilLanzaExcepcion() throws SinStockException {
		try 
		{
			ModeloAutomovil auto = mock(ModeloAutomovil.class);
			PlantaDeProduccion planta = mock(PlantaDeProduccion.class);
			Map<ModeloAutomovil, Integer> stock = new HashMap<ModeloAutomovil, Integer>();
			stock.put(auto, 2);
			fabrica.setStockTotalPorModelo(stock);
		
			fabrica.venderAutomovil(auto, 3, planta);
			
			fail("Se esperaba excepcion: SinStockException"); //verificar que lanza excepcion
		}
		
		catch(SinStockException e) {}	
	}
	
	/*
	 * Proposito: Testear que el mensaje update actualiza la informacion correctamente.
	 */
	@Test
	public void testUpdate() {
		
		ModeloAutomovil auto = mock(ModeloAutomovil.class);
		Map<ModeloAutomovil, Integer> stock = new HashMap<ModeloAutomovil, Integer>();
		stock.put(auto, 1);
		fabrica.setStockTotalPorModelo(stock);

		fabrica.update(auto, 4);
		
		assertTrue(fabrica.getStockTotalPorModelo().get(auto).equals(5));
	}
}
