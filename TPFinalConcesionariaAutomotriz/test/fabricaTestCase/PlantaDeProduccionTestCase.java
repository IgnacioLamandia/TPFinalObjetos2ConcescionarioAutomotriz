package fabricaTestCase;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import automovil.ModeloAutomovil;
import fabrica.Fabrica;
import fabrica.MiObserver;
import fabrica.PlantaDeProduccion;

//SUT = PlantaDeProduccion

public class PlantaDeProduccionTestCase {
	
	private PlantaDeProduccion planta;

	@Before
	public void setUp() throws Exception {
		
		planta = new PlantaDeProduccion();
	}
	
	//TESTS GETTERS y SETTERS
	@Test
	public void testGetYSetModelo() {
		
		@SuppressWarnings("unchecked")
		Map<ModeloAutomovil, Integer> modelos = mock(Map.class);
		
		planta.setStockPorModelo(modelos);
		
		assertEquals(planta.getStockPorModelo(), modelos);
	}
	
	@Test
	public void testGetYSetFabricas() {
		
		@SuppressWarnings("unchecked")
		List<MiObserver> fabricas = mock(List.class);
		
		planta.setObservers(fabricas);
		
		assertEquals(planta.getObservers(), fabricas);
	}
	
	@Test
	public void testStockDeModeloEnPlanta() {
		
		ModeloAutomovil auto = mock(ModeloAutomovil.class);
		
		Map<ModeloAutomovil, Integer> modelos = new HashMap<ModeloAutomovil, Integer>();
		modelos.put(auto, 5);
		
		planta.setStockPorModelo(modelos);
		
		assertTrue(planta.stockDeModeloEnPlanta(auto).equals(5));
	}
	
	/*
	 * Proposito: Comprobar que se registra una fabrica.
	 */
	@Test
	public void testRegisterYUnregisterFabrica() {
		Fabrica fabrica = mock(Fabrica.class);
		planta.register(fabrica);
		
		assertTrue(planta.getObservers().contains(fabrica));
		
		planta.unregister(fabrica);
		
		assertFalse(planta.getObservers().contains(fabrica));
	}
	
	/*
	 * Proposito: Comprobar que se agrega un modelo.
	 */
	@Test
	public void testAddModelo() {
		
		ModeloAutomovil auto = mock(ModeloAutomovil.class);
		
		planta.addModelo(auto);
		
		assertTrue(planta.getStockPorModelo().containsKey(auto));
	}
	
	/*
	 * Proposito: Comprobar que se modifica el stock al agregar un auto.
	 */
	@Test
	public void testAddStock() {
		
		ModeloAutomovil auto = mock(ModeloAutomovil.class);
		
		planta.addModelo(auto);
		planta.addStock(auto, 5);
		
		assertTrue(planta.getStockPorModelo().containsKey(auto));
		assertTrue(planta.getStockPorModelo().get(auto).equals(5));
	}
	
	/*
	 * Proposito: Comprobar que se modifica el stock al vender un auto.
	 */
	@Test
	public void testVenderAutomovil() {
		
		ModeloAutomovil auto = mock(ModeloAutomovil.class);
		Fabrica fabrica = mock(Fabrica.class);
		
		planta.register(fabrica);
		
		planta.addModelo(auto);
		planta.addStock(auto, 5);
		
		planta.venderAutomovil(auto, 2);
		
		assertTrue(planta.getStockPorModelo().get(auto).equals(3));
	}
}
