package automovilTestCase;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import automovil.ModeloAutomovil;

//SUT = ModeloAutomovil

public class ModeloAutomovilTestCase {

	private ModeloAutomovil automovilTest;

	@Before
	public void setUp() throws Exception {
		
		automovilTest = new ModeloAutomovil("modelo", new Date(2015), 
				   			      			5, true, 100000);
	}

	//TESTS GETTERS y SETTERS:
	@Test
	public void testGetYSetNombreModelo() {
		
		assertEquals(automovilTest.getNombreModelo(), "modelo");
		
		automovilTest.setNombreModelo("modelo2");
		assertEquals(automovilTest.getNombreModelo(), "modelo2");
	}
	
	@Test
	public void testGetYSetAnhoLanzamientoModelo() {
		
		assertTrue(automovilTest.getAnhoLanzamientoModelo().equals(new Date(2015)));
		
		automovilTest.setAnhoLanzamientoModelo(new Date(2016));
		assertTrue(automovilTest.getAnhoLanzamientoModelo().equals(new Date(2016)));
	}
	
	@Test
	public void testGetYSetCantidadDePuertas() {
		
		assertTrue((automovilTest.getCantidadPuertas()).equals(5));
		
		automovilTest.setCantidadPuertas(4);
		assertTrue((automovilTest.getCantidadPuertas()).equals(4));
	}
	
	@Test
	public void testGetYSetEsBase() {
		
		assertTrue(automovilTest.getEsBase());
		
		automovilTest.setEsBase(false);
		assertFalse(automovilTest.getEsBase());
	}
	
	@Test
	public void testGetYSetPrecio() {
		
		assertTrue((automovilTest.getPrecio()).equals(100000));
		
		automovilTest.setPrecio(200000);
		assertTrue((automovilTest.getPrecio()).equals(200000));
	}
	
}
