package financiamientoTestCase;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import financiamiento.FormaDeFinanciamiento100;

//SUT = FormaDeFinanciamiento100

public class FormaDeFinanciamiento100TestCase {
	
	private FormaDeFinanciamiento100 financiamiento;

	@Before
	public void setUp() throws Exception {
		
		financiamiento = new FormaDeFinanciamiento100(100);
	}

	//TESTS GETTERS y SETTERS
	@Test
	public void testGetPorcentajeEnEfectivo() {
		
		assertTrue(financiamiento.getPorcentajeEnEfectivo().equals(0));
	}
	
	@Test
	public void testGetPorcentajeEnCuotas() {
		
		assertTrue(financiamiento.getPorcentajeEnCuotas().equals(100));
	}
	
	@Test
	public void testGetYSetCuotas() {
		
		assertTrue(financiamiento.getCuotas().equals(100));
		
		financiamiento.setCuotas(200);
		assertTrue(financiamiento.getCuotas().equals(200));
		
	}
	
	/*
	 * Proposito: Testear que se calcula el monto a pagar para un precio determinado correctamente.
	 */
	@Test
	public void testCalcularMontoAPagar() {
		
		assertTrue(financiamiento.calcularMontoAPagar(100000).equals(0));
		
	}
	
	/*
	 * Proposito: Testear que se calcula la alicuota para un precio determinado correctamente.
	 */
	@Test
	public void testCalcularAlicuota() {
		
		assertTrue(financiamiento.calcularAlicuota(100000).equals(1000));
		
	}

}