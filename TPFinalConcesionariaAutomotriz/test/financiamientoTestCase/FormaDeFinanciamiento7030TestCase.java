package financiamientoTestCase;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import financiamiento.FormaDeFinanciamiento7030;

//SUT = FormaDeFinanciamiento7030

public class FormaDeFinanciamiento7030TestCase {

	private FormaDeFinanciamiento7030 financiamiento;

	@Before
	public void setUp() throws Exception {
		
		financiamiento = new FormaDeFinanciamiento7030(100);
	}

	//TESTS GETTERS y SETTERS
	@Test
	public void testGetPorcentajeEnEfectivo() {
		
		assertTrue(financiamiento.getPorcentajeEnEfectivo().equals(30));
	}
	
	@Test
	public void testGetPorcentajeEnCuotas() {
		
		assertTrue(financiamiento.getPorcentajeEnCuotas().equals(70));
	}
	
	@Test
	public void testGetYSetCuotas() {
		
		assertTrue(financiamiento.getCuotas().equals(100));
		
		financiamiento.setCuotas(200);
		assertTrue(financiamiento.getCuotas().equals(200));
		
	}
	
	/*
	 * Proposito: Testear que se calcula el monto a pagar para un precio determinado correctamente.
	 */
	@Test
	public void testCalcularMontoAPagar() {
		
		assertTrue(financiamiento.calcularMontoAPagar(100000).equals(30000));
		
	}
	
	/*
	 * Proposito: Testear que se calcula la alicuota para un precio determinado correctamente.
	 */
	@Test
	public void testCalcularAlicuota() {
		
		assertTrue(financiamiento.calcularAlicuota(140000).equals(2000));
		
	}

}