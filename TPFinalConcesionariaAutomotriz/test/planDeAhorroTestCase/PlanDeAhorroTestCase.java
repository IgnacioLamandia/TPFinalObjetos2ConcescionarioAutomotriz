package planDeAhorroTestCase;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import adjudicacion.ModalidadDeAdjudicacion;
import automovil.ModeloAutomovil;
import cliente.ClienteEnPlan;
import financiamiento.FormaDeFinanciamiento;
import pagos.ComprobanteDePago;
import planDeAhorro.PlanDeAhorro;

//SUT = PlanDeAhorro

public class PlanDeAhorroTestCase {
	
	private PlanDeAhorro plan;
	@Mock private ModeloAutomovil auto;
	@Mock private FormaDeFinanciamiento financiamiento;
	@Mock private ModalidadDeAdjudicacion adjudicacion;

	@Before
	public void setUp() throws Exception {
		
		auto = mock(ModeloAutomovil.class);
		financiamiento = mock(FormaDeFinanciamiento.class);
		adjudicacion = mock(ModalidadDeAdjudicacion.class);
		
		plan = new PlanDeAhorro(1, auto, financiamiento, adjudicacion);
	}

	//TESTS GETTERS y SETTERS
	@Test
	public void testGetYSetNumeroDeGrupo() {
		
		assertTrue(plan.getNumeroDeGrupo().equals(1));
		
		plan.setNumeroDeGrupo(2);
		assertTrue(plan.getNumeroDeGrupo().equals(2));
	}
	
	@Test
	public void testGetYSetModeloSuscripto() {
		
		assertEquals(plan.getModeloSuscripto(), auto);
		
		ModeloAutomovil auto2 = mock(ModeloAutomovil.class);
		
		plan.setModeloSuscripto(auto2);
		assertEquals(plan.getModeloSuscripto(), auto2);
	}
	
	@Test
	public void testGetYSetParticipantes() {
		
		@SuppressWarnings("unchecked")
		List<ClienteEnPlan> participantes = mock(List.class);
		
		plan.setParticipantes(participantes);
		assertEquals(plan.getParticipantes(), participantes);
	}
	
	@Test
	public void testGetYSetFinanciamiento() {
		
		assertEquals(plan.getFinanciamiento(), financiamiento);
		
		FormaDeFinanciamiento financiamiento2 = mock(FormaDeFinanciamiento.class);
		
		plan.setFinanciamiento(financiamiento2);
		assertEquals(plan.getFinanciamiento(), financiamiento2);
	}
	
	@Test
	public void testGetYSetModalidadDeAdjudicacion() {
		
		assertEquals(plan.getModalidadDeAdjudicacion(), adjudicacion);
		
		ModalidadDeAdjudicacion adjudicacion2 = mock(ModalidadDeAdjudicacion.class);
		
		plan.setModalidadDeAdjudicacion(adjudicacion2);
		assertEquals(plan.getModalidadDeAdjudicacion(), adjudicacion2);
	}
	
	@Test
	public void testGetYSetComprobantesDePago() {
		
		@SuppressWarnings("unchecked")
		List<ComprobanteDePago> comprobantes = mock(List.class);
		
		plan.setComprobantesDePago(comprobantes);
		assertEquals(plan.getComprobantesDePago(), comprobantes);
	}
	
	@Test
	public void testGetTotalDeCuotas() {
		
		when(financiamiento.getCuotas()).thenReturn(15);
		assertTrue(plan.getTotalDeCuotas().equals(15));
	}
	
	/*
	 * Proposito: Testear que se agrega un cliente a la lista de participantes.
	 */
	@Test
	public void testAgregarParticipante() {
		
		ClienteEnPlan cliente = mock(ClienteEnPlan.class);
		
		plan.agregarParticipante(cliente);
		assertTrue(plan.getParticipantes().contains(cliente));
	}
	
	/*
	 * Proposito: Testear que se agrega un comprobante a la lista de comprobantes.
	 */
	@Test
	public void testAgregarComprobanteDePago() {
		
		ComprobanteDePago comprobante = mock(ComprobanteDePago.class);
		
		plan.agregarComprobanteDePago(comprobante);
		assertTrue(plan.getComprobantesDePago().contains(comprobante));
	}
	
	/*
	 * Proposito: Testear que se agrega un comprobante a la lista de comprobantes.
	 */
	@Test
	public void testSeleccionarAdjudicatario() {
		
		ClienteEnPlan cliente = mock(ClienteEnPlan.class);
		plan.agregarParticipante(cliente);
		
		when(adjudicacion.seleccionarAdjudicatario(plan.getParticipantes())).thenReturn(cliente);
		
		assertTrue(plan.seleccionarAdjudicatario().equals(cliente));
		assertFalse(plan.getParticipantes().contains(cliente));
		
	}
	
	/*
	 * Proposito: Testear que se calcula correctamente el monto a pagar al momento de la entrega.
	 */
	@Test
	public void testGetCalcularMontoAPagar() {
		
		when(this.financiamiento.calcularMontoAPagar(0)).thenReturn(30000);
		assertTrue(plan.calcularMontoAPagar().equals(30000));
	}
	
	/*
	 * Proposito: Testear que se calcula la alicuota correctamente.
	 */
	@Test
	public void testGetCalcularAlicuota() {

		when(financiamiento.calcularAlicuota(0)).thenReturn(1000);
		assertTrue(plan.calcularAlicuota().equals(1000));
	}
	
	/*
	 * Proposito: Testear que se crea un comprobante de pago cuando un cliente paga una cuota y
	 * que se agrega a la lista de comprobantes.
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testEmitirComprobanteDePagoParaCliente() {
		
		ClienteEnPlan cliente = mock(ClienteEnPlan.class);
		
		when(plan.calcularAlicuota()).thenReturn(1000);
		
		assertEquals(plan.getComprobantesDePago().size(),0);//verifico que no hay comprobantes.
		
		plan.emitirComprobanteDePagoParaCliente(cliente, new Date(2015,1,1), 5000, 25000);
		
		assertEquals(plan.getComprobantesDePago().size(),1);//se agrega el nuevo comprobante.
		verify(cliente).pagarCuota(); //el cliente actualiza las cuotas pagadas.
	}
}