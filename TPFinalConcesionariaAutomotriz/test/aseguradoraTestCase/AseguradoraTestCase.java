package aseguradoraTestCase;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import aseguradora.Aseguradora;
import automovil.ModeloAutomovil;
import cliente.ClienteEnPlan;

//SUT = Aseguradora

public class AseguradoraTestCase {
	
	private Aseguradora aseguradora;

	@Before
	public void setUp() throws Exception {
		
		aseguradora = new Aseguradora(50, 50, 10, 5);
	}

	//TESTS GETTERS y SETTERS:
	@Test
	public void testGetYSetRecargoFijoPorEdad() {
		
		assertTrue((aseguradora.getRecargoFijoPorEdad()).equals(50));
		
		aseguradora.setRecargoFijoPorEdad(100);
		assertTrue((aseguradora.getRecargoFijoPorEdad()).equals(100));
	}
	
	@Test
	public void testGetYSetEdadLimiteSinRecargo() {
		
		assertTrue((aseguradora.getEdadLimiteSinRecargo()).equals(50));
		
		aseguradora.setEdadLimiteSinRecargo(55);
		assertTrue((aseguradora.getEdadLimiteSinRecargo()).equals(55));
	}
	
	@Test
	public void testGetYSetExtraPorEdad() {
		
		assertTrue((aseguradora.getExtraPorEdad()).equals(10));
		
		aseguradora.setExtraPorEdad(25);
		assertTrue((aseguradora.getExtraPorEdad()).equals(25));
	}
	
	@Test
	public void testGetYSetExtraPorValorAutomovil() {
		
		assertTrue((aseguradora.getExtraPorValorAutomovil()).equals(5));
		
		aseguradora.setExtraPorValorAutomovil(8);
		assertTrue((aseguradora.getExtraPorValorAutomovil()).equals(8));
	}
	
	/*
	 * Proposito: Testear que se calcula el seguro correctamente para un cliente que adquiere un automovil.
	 */
	@Test
	public void testCalcularSeguro() {
		
		ClienteEnPlan cliente = mock(ClienteEnPlan.class);
		ModeloAutomovil automovil = mock(ModeloAutomovil.class);
		
		when(cliente.calcularEdad()).thenReturn(55);
		when(automovil.getPrecio()).thenReturn(100000);
		
		Integer expected = aseguradora.calcularSeguro(cliente, automovil);
		
		assertTrue((expected).equals(5100));
	}
}