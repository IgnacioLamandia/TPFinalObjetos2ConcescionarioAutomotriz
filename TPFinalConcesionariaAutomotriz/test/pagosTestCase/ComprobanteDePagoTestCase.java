package pagosTestCase;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import cliente.ClienteEnPlan;
import pagos.ComprobanteDePago;
import planDeAhorro.PlanDeAhorro;

//SUT = ComprobanteDePago

public class ComprobanteDePagoTestCase {
	
	private ComprobanteDePago comprobanteDePago;
	@Mock private ClienteEnPlan participante;
	@Mock private PlanDeAhorro plan;

	@SuppressWarnings("deprecation")
	@Before
	public void setUp() throws Exception {
		
		participante = mock(ClienteEnPlan.class);
		when(participante.getNumeroDeCuotaAPagar()).thenReturn(5);
		
		plan = mock(PlanDeAhorro.class);
		when(plan.calcularAlicuota()).thenReturn(500);
		
		comprobanteDePago = new ComprobanteDePago(participante, plan, new Date(2016,5,5), 500, 100);
	}

	//TESTS GETTERS y SETTERS
	@Test
	public void testGetYSetParticipante() {
		
		ClienteEnPlan participante2 = mock(ClienteEnPlan.class);
		
		assertEquals(comprobanteDePago.getParticipante(), participante);
		
		comprobanteDePago.setParticipante(participante2);
		assertEquals(comprobanteDePago.getParticipante(), participante2);
	}
	
	@Test
	public void testGetYSetNumeroDeCuota() {
		
		assertTrue((comprobanteDePago.getNumeroDeCuota()).equals(5));
		
		comprobanteDePago.setNumeroDeCuota(6);
		assertTrue((comprobanteDePago.getNumeroDeCuota()).equals(6));
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testGetYSetFechaDePago() {
		
		assertEquals(comprobanteDePago.getFechaDePago(), new Date(2016,5,5));
		
		comprobanteDePago.setFechaDePago(new Date(2015,1,1));
		assertEquals(comprobanteDePago.getFechaDePago(), new Date(2015,1,1));
	}
	
	@Test
	public void testGetYSetAlicuota() {
		
		assertTrue((comprobanteDePago.getAlicuota()).equals(500));
		
		comprobanteDePago.setAlicuota(600);
		assertTrue((comprobanteDePago.getAlicuota()).equals(600));
	}
	
	@Test
	public void testGetYSetGastosAdministrativos() {
		
		assertTrue((comprobanteDePago.getGastosAdministrativos()).equals(500));
		
		comprobanteDePago.setGastosAdministrativos(600);
		assertTrue((comprobanteDePago.getGastosAdministrativos()).equals(600));
	}
	
	@Test
	public void testGetYSetSeguro() {
		
		assertTrue((comprobanteDePago.getSeguro()).equals(100));
		
		comprobanteDePago.setSeguro(300);
		assertTrue((comprobanteDePago.getSeguro()).equals(300));
	}
	
	/*
	 * Proposito: Testear que se calcula la alicuota correctamente.
	 */
	@Test
	public void testGetCalcularAlicuota() {

		assertTrue(plan.calcularAlicuota().equals(500));
	}
}