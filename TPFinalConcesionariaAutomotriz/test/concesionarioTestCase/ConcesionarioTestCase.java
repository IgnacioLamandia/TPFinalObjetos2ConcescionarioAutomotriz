package concesionarioTestCase;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;

import aseguradora.Aseguradora;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import automovil.ModeloAutomovil;
import cliente.Cliente;
import cliente.ClienteEnPlan;
import concesionario.Concesionario;
import cupon.CuponDeAdjudicacion;
import fabrica.Fabrica;
import fabrica.PlantaDeProduccion;
import financiamiento.FormaDeFinanciamiento;
import adjudicacion.ModalidadDeAdjudicacion;
import planDeAhorro.PlanDeAhorro;

//SUT = Concescionario

public class ConcesionarioTestCase {
	
	private Concesionario concesionario;
	
	@Mock private Fabrica fabrica;
	@Mock private Aseguradora aseguradora;
	
	@Mock private Cliente cliente1;
	@Mock private Cliente cliente2;
	@Mock private Cliente cliente3;
	
	private List<Cliente> clientes;
	
	@Mock private PlanDeAhorro plan1;
	@Mock private PlanDeAhorro plan2;
	@Mock private PlanDeAhorro plan3;
	
	private List<PlanDeAhorro> planes;
	
	@Mock private CuponDeAdjudicacion cupon1;
	@Mock private CuponDeAdjudicacion cupon2;
	@Mock private CuponDeAdjudicacion cupon3;
	
	private List<CuponDeAdjudicacion> cupones;

	@Before
	public void setUp() throws Exception {
		
		fabrica = mock(Fabrica.class);
		aseguradora = mock(Aseguradora.class);
		
		concesionario = new Concesionario(fabrica, aseguradora);
		
		cliente1 = mock(Cliente.class);
		cliente2 = mock(Cliente.class);
		cliente3 = mock(Cliente.class);
		
		clientes = new ArrayList<Cliente>();
		clientes.add(cliente1);
		clientes.add(cliente2);
		clientes.add(cliente3);
		
		plan1 = mock(PlanDeAhorro.class);
		plan2 = mock(PlanDeAhorro.class);
		plan3 = mock(PlanDeAhorro.class);
		
		planes = new ArrayList<PlanDeAhorro>();
		planes.add(plan1);
		planes.add(plan2);
		planes.add(plan3);
		
		cupon1 = mock(CuponDeAdjudicacion.class);
		cupon2 = mock(CuponDeAdjudicacion.class);
		cupon3 = mock(CuponDeAdjudicacion.class);
		
		cupones = new ArrayList<CuponDeAdjudicacion>();
		cupones.add(cupon1);
		cupones.add(cupon2);
		cupones.add(cupon3);
				
	}

	//TESTS GETTERS y SETTERS
	@Test
	public void testGetYSetFabrica() {
		
		Fabrica fabrica2 = mock(Fabrica.class);
		
		assertEquals(concesionario.getFabrica(), fabrica);
		
		concesionario.setFabrica(fabrica2);
		assertEquals(concesionario.getFabrica(), fabrica2);
	}
	
	@Test
	public void testGetYSetDistanciasAPlantas() {
		
		@SuppressWarnings("unchecked")
		HashMap<PlantaDeProduccion, Integer> distancias = mock(HashMap.class);
		
		concesionario.setDistanciasAPlantas(distancias);
		assertEquals(concesionario.getDistanciasAPlantas(), distancias);
	}
	
	@Test
	public void testGetYSetClientes() {
		
		concesionario.setClientes(clientes);
		assertEquals(concesionario.getClientes(), clientes);
	}
	
	@Test
	public void testGetYSetPlanes() {
		
		concesionario.setPlanes(planes);
		assertEquals(concesionario.getPlanes(), planes);
	}
	
	@Test
	public void testGetYSetCupones() {
		
		concesionario.setCupones(cupones);
		assertEquals(concesionario.getCupones(), cupones);
	}
	
	@Test
	public void testGetYSetAseguradora() {
		
		Aseguradora aseguradora2 = mock(Aseguradora.class);
		
		assertEquals(concesionario.getAseguradora(), aseguradora);
		
		concesionario.setAseguradora(aseguradora2);
		assertEquals(concesionario.getAseguradora(), aseguradora2);
	}
	
	@Test
	public void testAgregarCliente() {
		
		concesionario.agregarCliente(cliente1);
		assertTrue(concesionario.getClientes().contains(cliente1));
	}
	
	@Test
	public void testAgregarPlanDeAhorro() {
		
		concesionario.agregarPlanDeAhorro(plan1);
		assertTrue(concesionario.getPlanes().contains(plan1));
	}
	
	@Test
	public void testAgregarCupon() {
		
		concesionario.agregarCupon(cupon1);
		assertTrue(concesionario.getCupones().contains(cupon1));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testPlanesConMayorCantidadDeSuscriptos() {
		
		concesionario.setPlanes(planes);
		
		List<ClienteEnPlan> l1 = mock(List.class);
		List<ClienteEnPlan> l2 = mock(List.class);
		List<ClienteEnPlan> l3 = mock(List.class);
		
		when(plan1.getParticipantes()).thenReturn(l1);
		when(plan2.getParticipantes()).thenReturn(l2);
		when(plan3.getParticipantes()).thenReturn(l3);
		
		when(l1.size()).thenReturn(5);
		when(l2.size()).thenReturn(6);
		when(l3.size()).thenReturn(5);
		
		List<PlanDeAhorro> listaOrdenada = concesionario.planesConMayorCantidadDeSuscriptos();
		
		assertEquals(listaOrdenada.get(0), plan2);
		assertEquals(listaOrdenada.get(1), plan1);
		assertEquals(listaOrdenada.get(2), plan3);
	}
	
	@Test
	public void testEjecutarAdjudicacion() {

		Fabrica fabrica2 = new Fabrica();
		Aseguradora aseguradora2 = mock(Aseguradora.class);
		
		Concesionario concescionario2 = new Concesionario(fabrica2, aseguradora2);
		ModeloAutomovil auto = mock(ModeloAutomovil.class);
		FormaDeFinanciamiento financiamiento = mock(FormaDeFinanciamiento.class);
		ModalidadDeAdjudicacion adjudicacion = mock(ModalidadDeAdjudicacion.class);
		
		PlanDeAhorro plan = new PlanDeAhorro(15,auto,financiamiento,adjudicacion);
		
		ClienteEnPlan cliente1 = mock(ClienteEnPlan.class);
		ClienteEnPlan cliente2 = mock(ClienteEnPlan.class);
		ClienteEnPlan cliente3 = mock(ClienteEnPlan.class);
		List<ClienteEnPlan> participantes = new ArrayList<ClienteEnPlan>();
		participantes.add(cliente1);
		participantes.add(cliente2);
		participantes.add(cliente3);
		plan.setParticipantes(participantes);
		
		concescionario2.agregarPlanDeAhorro(plan);
		
		when(plan.seleccionarAdjudicatario()).thenReturn(cliente2);

		PlantaDeProduccion planta = new PlantaDeProduccion();
		planta.addModelo(auto);
		planta.addStock(auto, 50);
		
		fabrica2.addPlantaDeProduccion(planta);

		Map<PlantaDeProduccion, Integer> distancias = new HashMap<PlantaDeProduccion, Integer>();
		distancias.put(planta, 100);
		concescionario2.setDistanciasAPlantas(distancias);

		concescionario2.ejecutarAdjudicacion(plan);

		assertEquals(concescionario2.getPlanes().get(0).seleccionarAdjudicatario(), cliente2);

	}
	
	@Test
	public void testEjecutarAdjudicaciones() {
		Fabrica fabrica2 = new Fabrica();
		Aseguradora aseguradora2 = mock(Aseguradora.class);
		
		Concesionario concescionario2 = new Concesionario(fabrica2, aseguradora2);
		
		ModeloAutomovil auto = mock(ModeloAutomovil.class);

		FormaDeFinanciamiento financiamiento = mock(FormaDeFinanciamiento.class);
		ModalidadDeAdjudicacion adjudicacion = mock(ModalidadDeAdjudicacion.class);

		PlanDeAhorro plan1 = new PlanDeAhorro(15,auto,financiamiento,adjudicacion);
		PlanDeAhorro plan2 = new PlanDeAhorro(20,auto,financiamiento,adjudicacion);
		PlanDeAhorro plan3 = new PlanDeAhorro(25,auto,financiamiento,adjudicacion);
		
		ClienteEnPlan cliente1 = mock(ClienteEnPlan.class);
		ClienteEnPlan cliente2 = mock(ClienteEnPlan.class);
		ClienteEnPlan cliente3 = mock(ClienteEnPlan.class);
		
		List<ClienteEnPlan> participantes = new ArrayList<ClienteEnPlan>();
		participantes.add(cliente1);
		participantes.add(cliente2);
		participantes.add(cliente3);
		plan1.setParticipantes(participantes);
		plan2.setParticipantes(participantes);
		plan3.setParticipantes(participantes);
		
		concescionario2.agregarPlanDeAhorro(plan1);
		concescionario2.agregarPlanDeAhorro(plan2);
		concescionario2.agregarPlanDeAhorro(plan3);
		
		when(adjudicacion.seleccionarAdjudicatario(participantes)).thenReturn(cliente3);
		when(adjudicacion.seleccionarAdjudicatario(participantes)).thenReturn(cliente1);
		when(adjudicacion.seleccionarAdjudicatario(participantes)).thenReturn(cliente3);

		PlantaDeProduccion planta = new PlantaDeProduccion();
		planta.addModelo(auto);
		planta.addStock(auto, 50);
		
		fabrica2.addPlantaDeProduccion(planta);

		Map<PlantaDeProduccion, Integer> distancias = new HashMap<PlantaDeProduccion, Integer>();
		distancias.put(planta, 100);
	
		concescionario2.setDistanciasAPlantas(distancias);

		concescionario2.ejecutarAdjudicaciones();
		
		assertEquals(concescionario2.getPlanes().get(0).seleccionarAdjudicatario(), cliente3);
		assertEquals(concescionario2.getPlanes().get(1).seleccionarAdjudicatario(), cliente3);
		assertEquals(concescionario2.getPlanes().get(2).seleccionarAdjudicatario(), cliente3);
	}
	
	@Test
	public void testHayStockDeModelo() {
		
		ModeloAutomovil automovil = mock(ModeloAutomovil.class);
		
		concesionario.hayStockDeModelo(automovil, 5);
		
		verify(fabrica).hayStockDeModelo(automovil, 5);
	}

	@Test
	public void testPlantasConStockDeAutomovil() {
		
		ModeloAutomovil automovil = mock(ModeloAutomovil.class);
		
		concesionario.plantasConStockDeAutomovil(automovil, 5);
		
		verify(fabrica).plantasConStockDeModelo(automovil, 5);
	}
	
	@Test
	public void testPlantaMasCercanaConStock() {
		
		Fabrica fabrica2 = new Fabrica();
		Aseguradora aseguradora2 = mock(Aseguradora.class);
		
		Concesionario concescionario2 = new Concesionario(fabrica2, aseguradora2);
			HashMap<PlantaDeProduccion, Integer> distancias = new HashMap<PlantaDeProduccion, Integer>();
			PlantaDeProduccion planta1 = new PlantaDeProduccion();
			PlantaDeProduccion planta2 = new PlantaDeProduccion();
			PlantaDeProduccion planta3 = new PlantaDeProduccion();
			ModeloAutomovil auto = mock(ModeloAutomovil.class);
			
			fabrica2.addPlantaDeProduccion(planta1);
			fabrica2.addPlantaDeProduccion(planta2);
			fabrica2.addPlantaDeProduccion(planta3);
			fabrica2.addModeloAutomovil(auto);			
			planta3.register(fabrica);
			planta2.register(fabrica);
			planta1.register(fabrica);
			
			planta1.addStock(auto, 15);
			planta2.addStock(auto, 12);
			planta3.addStock(auto, 45);

			distancias.put(planta1, 100);
			distancias.put(planta2, 50);
			distancias.put(planta3, 300);

			concescionario2.setDistanciasAPlantas(distancias);
			
			assertEquals(concescionario2.plantaMasCercanaConStock(auto, 6), planta2);
	}
	
	@Test
	public void testCalcularGastosDeFletes() {
		
		Map<PlantaDeProduccion, Integer> distancias = new HashMap<PlantaDeProduccion, Integer>();
		PlantaDeProduccion planta = new PlantaDeProduccion();
		distancias.put(planta, 100);
		concesionario.setDistanciasAPlantas(distancias);
		assertTrue(concesionario.calcularGastosDeFlete(planta).equals(100));
	}

	@Test
	public void testGetStockTotalPorModelo() {
		
		Map<ModeloAutomovil, Integer> modelos = new HashMap<ModeloAutomovil, Integer>();
	
		when(this.fabrica.getStockTotalPorModelo()).thenReturn(modelos);

		assertEquals(concesionario.getStockTotalPorModelo(), modelos);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testEfectuarPagoDeCuotaDeCliente() {
		
		ClienteEnPlan participante = mock(ClienteEnPlan.class);
		ModeloAutomovil auto = mock(ModeloAutomovil.class);
			
		when(plan1.getModeloSuscripto()).thenReturn(auto);
		
		concesionario.efectuarPagoDeCuotaDeCliente(participante, plan1, new Date(2016,1,1), 500);
		
		verify(aseguradora).calcularSeguro(participante, auto);
		
		Integer seguro = aseguradora.calcularSeguro(participante, auto);
		verify(plan1).emitirComprobanteDePagoParaCliente(participante , new Date(2016,1,1), 500, seguro);

	}
}
