package cuponTestCase;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import automovil.ModeloAutomovil;
import cliente.ClienteEnPlan;
import cupon.CuponDeAdjudicacion;

//SUT = CuponDeAdjudicacion

public class CuponDeAdjudicacionTestCase {
	
	private CuponDeAdjudicacion cupon;
	@Mock private ClienteEnPlan adjudicatario;
	@Mock private ModeloAutomovil auto;

	@Before
	public void setUp() throws Exception {
		
		adjudicatario = mock(ClienteEnPlan.class);
		auto = mock(ModeloAutomovil.class);
		
		cupon = new CuponDeAdjudicacion(adjudicatario, auto, 1500);
	}

	//TESTS GETTERS y SETTERS
	@Test
	public void testGetYSetAdjudicatario() {
		
		ClienteEnPlan adjudicatario2 = mock(ClienteEnPlan.class);
		
		assertEquals(cupon.getAdjudicatario(), adjudicatario);
		
		cupon.setAdjudicatario(adjudicatario2);
		assertEquals(cupon.getAdjudicatario(), adjudicatario2);
	}
	
	@Test
	public void testGetYSetAutomovil() {
		
		ModeloAutomovil auto2 = mock(ModeloAutomovil.class);
		
		assertEquals(cupon.getAutomovil(), auto);
		
		cupon.setAutomovil(auto2);
		assertEquals(cupon.getAutomovil(), auto2);
	}
	
	@Test
	public void testGetYSetMontoAPagar() {
		
		assertTrue((cupon.getMontoAPagar()).equals(1500));
		
		cupon.setMontoAPagar(6000);
		assertTrue((cupon.getMontoAPagar()).equals(6000));
	}
	
}
