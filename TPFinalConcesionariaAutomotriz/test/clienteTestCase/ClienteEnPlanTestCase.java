package clienteTestCase;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import cliente.Cliente;
import cliente.ClienteEnPlan;

//SUT = ClienteEnPlan

public class ClienteEnPlanTestCase {
	
	private ClienteEnPlan clienteEnPlanTest;
	@Mock private Cliente cliente;
	@Mock private Cliente cliente2;
	
	@SuppressWarnings("deprecation")
	@Before
	public void setUp() throws Exception {
		
		cliente = mock(Cliente.class);
		cliente2 = mock(Cliente.class);
				
		clienteEnPlanTest = new ClienteEnPlan(cliente, new Date(2016,1,6));
	}

	@Test
	public void testGetYSetCliente() {
		
		assertEquals(clienteEnPlanTest.getCliente(), cliente);
		
		clienteEnPlanTest.setCliente(cliente2);
		assertEquals(clienteEnPlanTest.getCliente(), cliente2);
	}
	
	@Test
	public void testGetYSetCuotasPagadas() {
		
		assertTrue(clienteEnPlanTest.getCuotasPagadas().equals(0));
		
		clienteEnPlanTest.setCuotasPagadas(5);
		assertTrue(clienteEnPlanTest.getCuotasPagadas().equals(5));
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testGetYSetFechaIngresoPlan() {

		assertTrue(clienteEnPlanTest.getFechaIngresoPlan().equals(new Date(2016,1,6)));
		
		clienteEnPlanTest.setFechaIngresoPlan(new Date(2015,2,2));
		assertTrue(clienteEnPlanTest.getFechaIngresoPlan().equals(new Date(2015,2,2)));
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testGetFechaDeIngresoCliente() {
		
		when(cliente.getFechaDeIngresoCliente()).thenReturn(new Date(2015,1,6));
		assertTrue(clienteEnPlanTest.getFechaDeIngresoCliente().equals(new Date(2015,1,6)));
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testGetFechaDeNacimientoCliente() {
		
		when(cliente.getFechaDeNacimientoCliente()).thenReturn(new Date(1980,1,6));
		assertTrue(clienteEnPlanTest.getFechaDeNacimientoCliente().equals(new Date(1980,1,6)));
	}
	
	@Test
	public void testGetNumeroDeCuotaAPagarYPagarCuota() {
		
		assertTrue(clienteEnPlanTest.getNumeroDeCuotaAPagar().equals(1));
		
		clienteEnPlanTest.pagarCuota();
		assertTrue(clienteEnPlanTest.getNumeroDeCuotaAPagar().equals(2));
		
	}
	
	@Test
	public void testCalcularEdadCliente() {
		
		when(cliente.calcularEdad()).thenReturn(50);
		assertTrue(clienteEnPlanTest.calcularEdad().equals(50));
	}	

}
