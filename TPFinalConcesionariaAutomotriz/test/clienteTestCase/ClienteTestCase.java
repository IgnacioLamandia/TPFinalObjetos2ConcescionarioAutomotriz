package clienteTestCase;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import cliente.Cliente;

//SUT = Cliente

public class ClienteTestCase {
	
	private Cliente clienteTest;

	@SuppressWarnings("deprecation")
	@Before
	public void setUp() throws Exception {
		
		clienteTest = new Cliente("nombre", "apellido", "12345678",new Date(1980, 6, 2), 
				   			      "direccion", "mail", new Date(2016, 3, 6));
	}

	@Test
	public void testGetYSetNombreCliente() {
		
		assertEquals(clienteTest.getNombreCliente(), "nombre");
		
		clienteTest.setNombreCliente("nombre2");
		assertEquals(clienteTest.getNombreCliente(), "nombre2");
	}
	
	@Test
	public void testGetYSetApellidoCliente() {
		
		assertEquals(clienteTest.getApellidoCliente(), "apellido");
		
		clienteTest.setApellidoCliente("apellido2");
		assertEquals(clienteTest.getApellidoCliente(), "apellido2");
	}
	
	@Test
	public void testGetYSetDniCliente() {
		
		assertEquals(clienteTest.getDniCliente(), "12345678");
		
		clienteTest.setDniCliente("87654321");
		assertEquals(clienteTest.getDniCliente(), "87654321");
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testGetYSetFechaDeNacimientoCliente() {
		
		assertTrue(clienteTest.getFechaDeNacimientoCliente().equals(new Date(1980,6,2)));
		
		clienteTest.setFechaDeNacimientoCliente(new Date(1980,2,2));
		assertTrue(clienteTest.getFechaDeNacimientoCliente().equals(new Date(1980,2,2)));
	}
	
	@Test
	public void testGetYSetDireccionCliente() {
		
		assertEquals(clienteTest.getDireccionCliente(), "direccion");
		
		clienteTest.setDireccionCliente("direccion2");
		assertEquals(clienteTest.getDireccionCliente(), "direccion2");
	}
	
	@Test
	public void testGetYSetMailCliente() {
		
		assertEquals(clienteTest.getMailCliente(), "mail");
		
		clienteTest.setMailCliente("mail2");
		assertEquals(clienteTest.getMailCliente(), "mail2");
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testGetYSetFechaDeIngresoCliente() {
		
		assertTrue(clienteTest.getFechaDeIngresoCliente().equals(new Date(2016,3,6)));
		
		clienteTest.setFechaDeIngresoCliente(new Date(2016,2,2));
		assertTrue(clienteTest.getFechaDeIngresoCliente().equals(new Date(2016,2,2)));
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testCalcularEdadCliente() {
		clienteTest.setFechaDeNacimientoCliente(new Date(1980,6,27));
		assertTrue(clienteTest.calcularEdad().equals(36));
	}	
}