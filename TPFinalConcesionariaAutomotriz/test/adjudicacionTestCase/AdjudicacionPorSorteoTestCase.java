package adjudicacionTestCase;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import adjudicacion.ModalidadDeAdjudicacionPorSorteo;
import cliente.ClienteEnPlan;

	public class AdjudicacionPorSorteoTestCase {
		
		private ModalidadDeAdjudicacionPorSorteo sorteo;
		
		@Mock private ClienteEnPlan cliente;
		
		private List<ClienteEnPlan> participantes;

		@Before
		public void setUp() throws Exception 
		{
			sorteo = new ModalidadDeAdjudicacionPorSorteo();
			
			participantes = new ArrayList<ClienteEnPlan>();
			participantes.add(cliente);
			
		}
		
		/*
		 * Proposito: Testear que obtengo un cliente al azar.
		 */
		@Test
		public void testSeleccionarAdjudicatarioDondeObtengoUnParticipanteAlAzar() {
			
			assertEquals(this.sorteo.seleccionarAdjudicatario(participantes), cliente);

		}

	}