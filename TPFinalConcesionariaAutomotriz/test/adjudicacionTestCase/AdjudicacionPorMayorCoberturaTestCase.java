package adjudicacionTestCase;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import adjudicacion.ModalidadDeAdjudicacionPorMayorCobertura;
import cliente.ClienteEnPlan;

//SUT = AdjudicacionPorMayorCoberturaTestCase

public class AdjudicacionPorMayorCoberturaTestCase {
	
	private ModalidadDeAdjudicacionPorMayorCobertura mayorCobertura;
	
	@Mock private ClienteEnPlan c1;
	@Mock private ClienteEnPlan c2;
	@Mock private ClienteEnPlan c3;
	
	private List<ClienteEnPlan> participantes;

	@Before
	public void setUp() throws Exception 
	{		
		mayorCobertura = new ModalidadDeAdjudicacionPorMayorCobertura();
		
		c1 = mock(ClienteEnPlan.class);
		c2 = mock(ClienteEnPlan.class);
		c3 = mock(ClienteEnPlan.class);
		
		participantes = new ArrayList<ClienteEnPlan>();
		participantes.add(c1);
		participantes.add(c2);
		participantes.add(c3);
		
	}
	
	/*
	 * Proposito: Testear que obtengo un cliente por mayor proporcion de pago..
	 */
	@Test
	public void testSeleccionarAdjudicatarioPorMayorProporcionDePago() {
	when(c1.getCuotasPagadas()).thenReturn(10);
	when(c2.getCuotasPagadas()).thenReturn(15);
	when(c3.getCuotasPagadas()).thenReturn(12);
	
	assertEquals(this.mayorCobertura.seleccionarAdjudicatario(participantes), c2);
	
	}
	
	/*
	 * Proposito: Testear que obtengo un cliente porque tiene mayor antiguedad.
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testSeleccionarAdjudicatarioPorMayorAntiguedad() {
	when(c1.getCuotasPagadas()).thenReturn(10);
	when(c2.getCuotasPagadas()).thenReturn(10);
	when(c3.getCuotasPagadas()).thenReturn(10);
	
	when(c1.getFechaDeIngresoCliente()).thenReturn(new Date(2016,1,1));
	when(c2.getFechaDeIngresoCliente()).thenReturn(new Date(2015,1,1));
	when(c3.getFechaDeIngresoCliente()).thenReturn(new Date(2016,4,1));
	
	assertEquals(this.mayorCobertura.seleccionarAdjudicatario(participantes), c2);
	
	}
	
	/*
	 * Proposito: Testear que obtengo un cliente porque tiene mayor antiguedad en el plan.
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testSeleccionarAdjudicatarioPorMayorAntiguedadEnPlan() {
	when(c1.getCuotasPagadas()).thenReturn(10);
	when(c2.getCuotasPagadas()).thenReturn(10);
	when(c3.getCuotasPagadas()).thenReturn(10);
	
	when(c1.getFechaDeIngresoCliente()).thenReturn(new Date(2016,1,1));
	when(c2.getFechaDeIngresoCliente()).thenReturn(new Date(2016,1,1));
	when(c3.getFechaDeIngresoCliente()).thenReturn(new Date(2016,1,1));

	when(c1.getFechaIngresoPlan()).thenReturn(new Date(2016,1,6));
	when(c2.getFechaIngresoPlan()).thenReturn(new Date(2016,1,5));
	when(c3.getFechaIngresoPlan()).thenReturn(new Date(2016,1,8));
	
	assertEquals(this.mayorCobertura.seleccionarAdjudicatario(participantes), c2);
	
	}

	/*
	 * Proposito: Testear que obtengo el cliente con mayor proporcion de pago.
	 */
	@Test
	public void testObtengoElParticipanteDeMayorCoberturaPorMayorProporcionDePago() {
		
		when(c1.getCuotasPagadas()).thenReturn(10);
		when(c2.getCuotasPagadas()).thenReturn(15);
		when(c3.getCuotasPagadas()).thenReturn(7);
		
		assertEquals(this.mayorCobertura.mayorProporcionDePago(participantes).get(0), c2);

	}
	
	/*
	 * Proposito: Testear que obtengo el cliente con mayor antiguedad.
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testObtengoElParticipanteDeMayorCoberturaPorMayorAntiguedad() {
		
		when(c1.getFechaDeIngresoCliente()).thenReturn(new Date(2016,1,6));
		when(c2.getFechaDeIngresoCliente()).thenReturn(new Date(2016,1,5));
		when(c3.getFechaDeIngresoCliente()).thenReturn(new Date(2016,1,8));
		
		assertEquals(this.mayorCobertura.mayorAntiguedad(participantes).get(0), c2);

	}
	
	/*
	 * Proposito: Testear que obtengo el cliente con mayor antiguedad en el plan.
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testObtengoElParticipanteDeMayorCoberturaPorMayorAntiguedadEnPlan() {
		
		when(c1.getFechaIngresoPlan()).thenReturn(new Date(2016,1,6));
		when(c2.getFechaIngresoPlan()).thenReturn(new Date(2016,1,5));
		when(c3.getFechaIngresoPlan()).thenReturn(new Date(2016,1,8));
		
		assertEquals(this.mayorCobertura.mayorAntiguedadEnPlan(participantes).get(0), c2);

	}

}